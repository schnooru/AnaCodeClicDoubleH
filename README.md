Analysis code for the double H analysis

# Structure and setup

```
source /cvmfs/clicdp.cern.ch/iLCSoft/builds/2020-02-07/x86_64-slc6-gcc62-opt/init_ilcsoft.sh
source setup.sh
```

# How to run the nominal analysis

The nominal analysis is located in `DoubleH`.

*RUN:*
This runs on the central analysis ntuples, i.e. the TMVA file for the backgrounds and the AnaChain files for the signals, and produces ntuples with the observables used in the analysis, for different selection stages for normalization purposes:
```
python runOverDoubleH.py ../Configs/runConfig_RUN_nocuts_2018.json
python runOverDoubleH.py ../Configs/runConfig_RUN_presel_2018.json
python runOverDoubleH.py ../Configs/runConfig_RUN_selection_2018.json
```

*PROCESS:*
This processes the ntuples given above, applies the correct normalization, and fills histograms which are then saved to ROOT file:
```
python processHistosDoubleH.py ../Configs/runConfig_PROCESS_2018.json
```

*FIT and PLOT:*
This takes the histograms from the ROOT file above and makes plots of signals and backgrounds (various possibilities). It also performs the template fit for the sensitivitiy study:
```
python templateFitDoubleH.py ../Configs/runConfig_PROCESS_2018.json
```

*DIFFERENTIAL:*
This plots differential distributions:
```
python createDifferential.py ../Configs/runConfig_PROCESS_2018.json
```


There are a few helper codes in `DoubleH/`:


## Code for smaller studies
For some smaller studies that do not require the full set of signals and backgrounds but just selected parts, the code is not located in the central `DoubleH` directory but rather in `Referee_Checks_Polar_ZnunuHH` and in `TheoryLevel`.

### Checks of the kinematics dependence on polarisation and the Z(nunu)HH contribution
+ For comparisons of the WW kinematics in dependence on the polarisation, use `python fillHistosMWW.py` and `plotMWW.py`. Within the macros, configure the histograms.

+ For efficiency study of the BDT cut on the Z(nunu)HH component, use `python plot_histos_divide.py`. Also here, file configuration is inside the small macro.

### Plots of kinematics for different signals on Generator level

Located in `TheoryLevel`. Here, I extract truth information from some existing GEN files of the signal.

Steps:
+ first to create histograms:
`python MhhTheory.py`
+ merge /hadd them:
` hadd histofile_7181.root histofile_7181_*.root`
+  then to plot them with correct normalisation:
`python truthMCPlots.py`


# Submodule git instructions
Uses the submodule ClicPyAna from https://gitlab.cern.ch:8443/schnooru/ClicPyAna (that I also use in my other codes)


Check the .gitmodules file which contains the submodule information.

More info on submodules: https://git-scm.com/book/en/v2/Git-Tools-Submodules

## How to add a submodule to a repo
`git submodule add https://:@gitlab.cern.ch:8443/schnooru/ClicPyAna.git`
## How to update it if there is a change in the central repo?
*Fetch and merge locally:*

If you want to check for new work in a submodule, you can go into the directory and run git fetch and git merge the upstream branch to update the local code:
   
```
cd ClicPyAna
git fetch
```   
this updates the origin/master, such that then it is possible to fast-forward by merging:
   
```
git merge origin/master
```
Go back to the main project:
`../`

Then, need to commit in order to update the origin (repo) to the newest commit of the submodule
```
git add ClicPyAna
git commit -m "updated ClicPyAna submodule"
git push -u origin master
```   

*Alternatively*
`git submodule update ClicPyAna`


## How to push changes in the local submodule up to the central submodule's repo
   
Need to tell the submodule a branch, otherwise it is in detached state:
```
cd ClicPyAna
git checkout master
```
then add, commit, and push in the following way:

```
cd /path/to/outer/repo/<submodule>
```
make changes
```
git add <changed.file>
git commit -m "changed file changed.file"
git push origin master # or wherever
```
doing then
```cd  /path/to/outer/repo/
git status
```
says:
`modified: <submodule> (new commits)`

now one needs to tell the outer repo that the current newest commit of <submodule> should be used for the outer repo:
```
cd  /path/to/outer/repo/
git add <submodule>
git commit -m "let's update the <submodule> to the newest commit"
git push origin master
```
If there are other clones of this outer repo, there one has to do
`git submodule update`
(I think inside the <submodule> directory )

