# Submit jobs on stdhep files
# ulrike.schnoor@cern.ch



import os

from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.DiracILC import  DiracILC
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import *
from ILCDIRAC.Interfaces.API.NewInterface.Applications import *

repositoryfile ="testrepository.rep"
dsid = 6023
inputFiles = open("file_list_{}.list".format(dsid))

dirac = DiracILC(True,repositoryfile)

j=0
#create one job per inputFile
for inFile in inputFiles.readlines():
    j+=1
    inFile=inFile.strip("\n")
    print("Submitting job number {}".format(j))
    outputFileName = "Mhh_{}_{}.root".format(dsid, j)
    job = UserJob()
    #job.setInputSandbox("LFN:/ilc/user/s/schnooru/DoubleHiggs-TheoryMhh/lib.tar.gz")
    #local:
    job.setInputSandbox("lib.tar.gz")



    converter = RootScript()
    converter.setScript("stdhepjob")
    converter.setArguments("{} out.slcio -1 ".format(inFile))
    job.append(converter)
    job.setInputData([inFile, "out.slcio"])

    
    script = RootScript()
    script.setVersion("ILCSoft-2018-02-08_gcc62")
    script.setScript ("python")
    script.setArguments("MhhTheory.py")
    #script.setArguments("MhhTest.py")
    script.setOutputFile(outputFileName)
    job.setOutputSandbox(["*.log","*.sh"])
    job.setOutputData([outputFileName], OutputSE="CERN-DST-EOS")

    res =  job.append(script)



    if not res['OK']:
        print res['Message']
        exit()
    job.setJobGroup("")
    #job.submit(dirac)
    job.submit(dirac,  mode="local")

print ("Repository file: {}".format(repositoryfile))
