from pyLCIO import IOIMPL

from ROOT import TCanvas, TH1D, TF1, TLorentzVector, TVector3


# create a reader and open an LCIO file
reader = IOIMPL.LCFactory.getInstance().createLCReader()
#reader.open( 'test.slcio' )
#reader.open('/afs/cern.ch/user/s/schnooru/CLICwork/Delphes_Files/hzqq_Rec/hzqq_dst_2558_1.slcio')

#reader.open("/afs/cern.ch/user/s/schnooru/CLIC/Analysis/Software/Marlin/outputfile.slcio")

reader.open("/home/schnooru/Projects/Delphes/CLICdet-Delphes/Local_umQ_F2_JobDir/")


# create a canvas for the histogram
canvas = TCanvas( 'aCanvas', 'my Canvas', 800, 700 )
# create a histogram for the hit energies
MjjHistogram = TH1D( 'aHistogram', 'Mjj;Mjj[GeV];Entries', 100, 0., 500. )

# loop over all events in the file
for event in reader:
    # get the collection from the event
    #jetCollection = event.getCollection( "JetOut")
    jetCollection = event.getCollection("Jet_VLC_R10_N4")
    dijet, jetmom1, jetmom2 =  TLorentzVector(),  TLorentzVector(), TLorentzVector()

    #fill  M(jj) for all pairs
    for jet1 in jetCollection:
        for jet2 in jetCollection:
            if not jet1==jet2:
                jetmom1.SetPxPyPzE(jet1.getMomentum()[0],jet1.getMomentum()[1],jet1.getMomentum()[2] , jet1.getEnergy() )
                jetmom2.SetPxPyPzE(jet2.getMomentum()[0],jet2.getMomentum()[1],jet2.getMomentum()[2] , jet2.getEnergy() )
                dijet = jetmom1 + jetmom2
                MjjHistogram.Fill( dijet.M() )
                                

reader.close()

# draw the histogram
MjjHistogram.Draw()
canvas.Update()
raw_input()
