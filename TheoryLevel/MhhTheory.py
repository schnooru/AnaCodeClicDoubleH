#!/cvmfs/clicdp.cern.ch/DIRAC/Linux_x86_64_glibc-2.12/bin/python
from pyLCIO import IOIMPL
import sys
from ROOT import TCanvas, TH1D, TF1, TLorentzVector, TVector3, TFile
from math import cos
    
def FillHistosFromEvents(prodid, reader ):
    histoname = "SumMj_{}".format(prodid)
    histoname_pt_b = "PT_b_{}".format(prodid)
    histoname_E_b = "E_b_{}".format(prodid)
    histoname_abs_cos_theta_b = "abs_cos_theta_b_{}".format(prodid)
    # create a histogram for the hit energies
    MhhHistogram = TH1D( histoname, 'Mhh;Mhh[GeV];Entries', 100, 0., 3000. )
    PT_b_Histogram = TH1D ( histoname_pt_b, 'PT_b;PT_b[GeV];Entries', 100, 0, 800. )
    E_b_Histogram = TH1D(histoname_E_b, 'E_b;E_b[GeV];Entries', 100, 0, 1000.)
    cos_theta_b_Histogram = TH1D(histoname_abs_cos_theta_b, '|cos(#theta)_b|[GeV];|cos(#theta)_b|[GeV];Entries', 20, 0, 1)
    
    nevent=0
    # loop over all events in the file
    for event in reader:
        nevent+=1
        # get the collection from the event
        #jetCollection = event.getCollection( "JetOut")
        truthparticleCollection = event.getCollection("MCParticlesSkimmed")
        higgs1, higgs2, dihigss =  TLorentzVector(),  TLorentzVector(), TLorentzVector()
        higgscandidatelist= []
        higgses = []

        b_quarks = []
        

        #get the particles with pdgid 25 (higgs) which have 2 parents and one daughter
        nb = 0 #number of bs. Use the first 4 bs only.
        for particle in truthparticleCollection:
            PDGID = particle.getPDG ()
            if  PDGID == 25:
                higgscandidatelist.append(particle)
            if PDGID == 5 and nb <4 :
                b_quarks.append(particle)
                nb += 1
        #print("Found {} candidates for higgses".format(len(higgscandidatelist)))
        for candidate in higgscandidatelist:
            if len(candidate.getParents()) == 2 and len(candidate.getDaughters()) ==1:
                higgses.append(candidate)


        if len(higgses) !=2:
            print("WARNING found not 2 but {} higgses".format(len(higgses)))

    
        higgs1.SetPxPyPzE(higgses[0].getMomentum()[0],higgses[0].getMomentum()[1],higgses[0].getMomentum()[2] , higgses[0].getEnergy()    )
    
    
        higgs2.SetPxPyPzE(higgses[1].getMomentum()[0],higgses[1].getMomentum()[1],higgses[1].getMomentum()[2] , higgses[1].getEnergy()    )
        MhhHistogram.Fill( (higgs1+higgs2).M())


        if nb == 4:
            for b in b_quarks:
                #print b.getMomentumVec().Pt()
                PT_b_Histogram.Fill(b.getMomentumVec().Pt())
                E_b_Histogram.Fill(b.getLorentzVec().E())
                cos_theta_b_Histogram.Fill(abs(cos(b.getMomentumVec().Theta())))

    reader.close()


    
    return nevent , [MhhHistogram, PT_b_Histogram, E_b_Histogram, cos_theta_b_Histogram]

def FileNumberFromName(filename):
    print filename
    return

def main(*args):

    # create a reader and open an LCIO file
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    prodids = ["7169","7175"]
    prodids = ["7179","7181"]
    prodids = ["7183","7189"]
    prodids = ["8185","8209"]
    prodids = ["8201","7564"]
    prodids = ["7181"]


    for i in prodids:
        samplenamesfile = "fileslocal_{}.list".format(i)
        f = open(samplenamesfile,'r')
        nfile=0
        for line in f:
            nfile+=1
            print("Opening file {}".format(line.strip("\n")))
            reader.open(line.strip("\n").strip("\n"))
            outfile = "HistoFiles/histofile_{}_{}.root".format(i,nfile)
            hfile = TFile.Open(outfile,"RECREATE")
            nevents, histos = FillHistosFromEvents(i, reader)
            for histo in histos:
                histo.Write()

            print("Writing histogram to {}".format(outfile))
            hfile.Close()
        f.close()

    return




if __name__ == "__main__":
    main(*sys.argv)
