import csv
import os
import sys
import json

dsidcsv = "AllDsids.csv"



def FindInfoInOutput(outputstringlist,info):
    """ outputstringlist must already be separated etc"""
    info_out = "info {} not found".format(info)
    #print outputstringlist
    for i in outputstringlist:
        i_split = i.strip().split(":")
        #print i_split[0]
        if i_split[0]== info:
            info_out = i_split[1]
    return info_out



def ListOutputstringUseful(outputstring):
    inputdataquery=outputstring.strip().replace("InputDataQuery","").replace("{","").replace("\'","").replace("}","").strip().replace(": Datatype","Datatype").split(",")

    return inputdataquery



def RetrieveQuery(dsid  ):
    inputdataquery=os.popen("dirac-ilc-get-info -p {} | grep InputDataQuery".format(dsid)).read()
    outputstringlist = ListOutputstringUseful(inputdataquery)
    return outputstringlist


def GetDatatypeProdid(dsid,outputstringlist  ):
    
    #print outputstringlist
    datatype = FindInfoInOutput(outputstringlist, "Datatype").strip()
    prodID   = FindInfoInOutput(outputstringlist, "ProdID").strip()

    return {"Datatype": datatype, "ProdID": prodID}

def FindDsidsGenlevel():

    dict_DSIDs = {}
    with open(dsidcsv, 'rb') as csvfile:
        dsidreader = csv.reader(csvfile, delimiter=',')
        for row in dsidreader:
            if row[0].startswith("#"):
                continue
            #print ', '.join(row)
            dict_DSIDs[row[0]] = {}
    print( "Running over the following prodIDs: {}".format(", ".join(dict_DSIDs.keys())))


    for dsid in dict_DSIDs.keys():
        print("finding gen level ancestor for {}".format(dsid))
        query_out = RetrieveQuery(dsid  )
        #print("query_out: {}".format( query_out))
        datatype = GetDatatypeProdid(dsid ,query_out )["Datatype"]
        prodID   = GetDatatypeProdid(dsid, query_out )["ProdID"]
        #print dsid, datatype, prodID
        
        if datatype == "SIM":
            dict_DSIDs[dsid]["SIM"] = prodID
            query_out = RetrieveQuery(prodID  )
            datatype = GetDatatypeProdid( prodID,query_out  )["Datatype"]
            prodID   = GetDatatypeProdid( prodID,query_out  )["ProdID"]
        #now, the new datatype should already be gen.
        if datatype == "gen" or datatype == "info Datatype not found":
            dict_DSIDs[dsid]["gen"] = prodID
        #if the new datatype is still not gen:
        else:
            print("gen not yet found, second is datatype: {}".format(datatype))
            datatype = GetDatatypeProdid( prodID  )["Datatype"]
            prodID   = GetDatatypeProdid( prodID  )["ProdID"]
            if datatype == "gen" or datatype == "info Datatype not found":
                dict_DSIDs[dsid]["gen"] = prodID
        print dict_DSIDs


    print("Final dictionary of the prodids:")
    print dict_DSIDs
    return

def main(*args):
    #this is already done, takes a while:
    #FindDsidsGenlevel()


    #now, use the dsdis.json for writing the gen level dsids to another file
    dsid_out = open ("AllDsidsGen.csv","w")
    dsid_out.write("#prodID reco, prodID gen\n")
    with open('dsids.json') as f:
        dsiddict = json.load(f)

    print dsiddict
    for dsid_reco in dsiddict.keys():
        dsid_out.write("{}, {}\n".format(dsid_reco, dsiddict[dsid_reco]["gen"] ))
    print("writing to file {}".format(dsid_out.name))
    dsid_out.close()



if __name__ == "__main__":
    main(*sys.argv)

