"""Reads the truth MC files to make correctly normalized plots"""
import sys
from ROOT import TFile, gStyle, kOrange, kMagenta, kGreen, kPink, kRed, kBlue, kBlack, kCyan, kViolet, TLegend
from HHTools import dictAnaSample_FromCSV, ReadCsvToDict_floats
from clicpyana.PlottingTools import Plotter


def main(*args):
    #Open the HistoFiles/SignalsSumMj.root
    #extract the relevant histograms from it that correspond to the ones listed in selectedDSTs_Papercomparisons.csv
    #Scale them, then plot them probably using some of the pre-made plotting styles
    mHHfile = TFile.Open("HistoFiles/SignalsSumMj.root")
    mHHfile.ls()

    gStyle.SetLabelSize(0.04,"xyz")
    gStyle.SetLineWidth(3)


    gStyle.SetPadTickX(1)
    gStyle.SetPadTickY(1)

    #create a sample dict from the csv
    samplesDict = dictAnaSample_FromCSV( "selectedDSTs_Papercomparisons.csv" )
    print samplesDict
    canv = Plotter.createCanvas()
    histlist=[]


    signals_colors = [ kCyan  ,kGreen+3,kOrange+7,kViolet+2, kCyan, kBlue+2 ]
    linedict = {"8185": {"color": kCyan, "style": 4},
                "7181": {"color": kGreen+3 , "style": 2},
                "8201": {"color": kOrange+7, "style":6},
                "7564": {"color":kViolet+1 , "style":5},
                "7183": {"color":kBlue+2 , "style":3}
            }
#to plot all:    DSID = samplesDict.keys()
# to compare HHH and HHWW:     DSID= ["7181","7183","8201"]:
#to compare HHH smaller/large SM:  DSID= ["8185","7181","8201","7564"]:
    DSIDs = ["8185","7181","8201","7564"]
    #DSIDs= ["7181","7183","8201"]
    DSIDs= ["7181"]

    #signals_linestyle = [4,2,6,3,6]

    leg = TLegend(0.35,0.6,0.87,0.87)
    leg.SetBorderSize(0)
    leg.SetFillStyle(0) #transparent

    for DSID in DSIDs:
        #mHH_hist= mHHfile.Get("SumMj_{}".format(DSID))
        mHH_hist= mHHfile.Get("PT_b")
        if not mHH_hist:
            print( "histogram for {} not yet available".format(DSID))
            continue
        print mHH_hist.Integral()
        sigma =   samplesDict[DSID]["xsec"]
        mHH_hist.Rebin(4)
        mHH_hist.SetStats(0)
        mHH_hist.SetLineWidth(4)
        mHH_hist.Scale(sigma/mHH_hist.Integral())
        print("{} integral: {}".format(DSID,mHH_hist.Integral()))
        mHH_hist.SetLineColor(linedict[DSID]["color"])
        mHH_hist.SetLineStyle(linedict[DSID]["style"])
        mHH_hist.GetXaxis().SetTitle("M_{HH} [GeV]")
        xaxis = Plotter.setFeaturesAxis(mHH_hist.GetXaxis(), 1.1,0.06,0.06)
        binwidth = int(xaxis.GetBinWidth(0))
        mHH_hist.GetYaxis().SetTitle("d#sigma/d {} [fb/{} GeV]".format("M_{HH}",binwidth))
        #mHH_hist.GetYaxis().SetTitle("#sigma [fb]")
        yaxis = Plotter.setFeaturesAxis(mHH_hist.GetYaxis(), 1.4,0.06,0.06)
        kappaHHH=samplesDict[DSID]["couplings"][0]
        kappaHHWW=samplesDict[DSID]["couplings"][1]
        leg.AddEntry(mHH_hist,"{} = {}; {} = {}".format("#kappa_{HHH}",kappaHHH,  "#kappa_{HHWW}" ,kappaHHWW)  ,"l")
        histlist.append(mHH_hist)
        
    Plotter.plotComparison(canv,histlist,maxfact=1.4,legendymin=0.7,textsize=0.05)
    leg.Draw()
    canv.SetLeftMargin(0.18)
    canv.Modified()
    canv.Update()
    canv.SaveAs("{}_truthSignal_with_{}.pdf".format("SumMj","_".join(map(str,DSIDs))))
    raw_input()
    mHHfile.Close()

    
    return


if __name__ == "__main__":
    main(*sys.argv)
