import csv
import sys,os
from clicpyana.ReadingTools import ReaderTools
from ROOT import TFile, TH1F
import json



def dictCouplingsCSV(csvfilename):
    couplDict = {}
    csvfile = open(csvfilename,'rb')
    reader = csv.reader(csvfile)
    for line in reader:
        if len(line)>=5:
            if line[0].startswith("#"): continue
            #ProdID : [ gHHH, gHHWW, xsec, Nsample ]
            couplDict[int(line[0])] = [float(line[1]),float(line[2]), float(line[3]), float(line[4])]
   
    return couplDict

def findAnaChainFileInCheckDir(checkdir, dsid):
    potentialpath = "{}/anaChain_CLIC_ILD_4jets_{}.root".format(checkdir,dsid)
    if os.path.exists(potentialpath):
        return True, potentialpath
    else:
        return False, "NOT FOUND: {}".format(potentialpath)


def getCouplingsFromCSV_DSID(dsid, couplDict):
    return [couplDict[dsid][0],couplDict[dsid][1]]

def TablePrintout(SignalFiles_dict):
    """ this assumes a dict of the form

    <DSID> : [gHHH, gHHWW, xsec, Nsample, filename]
    """
    table = ""
    table += "{:<6} {:<10} {:<10} {:<152} {:<7}\n".format("prodID","gHHH", "gHHWW", "Filename","NStart")
    for ID in SignalFiles_dict.keys():
        infolist =  SignalFiles_dict[ID]
        table+=  "{:<6} {:<10} {:<10} {:<152} {:<7}\n".format(ID,infolist[0], infolist[1], infolist[4], infolist[3])
    return(table)

def FindAllSignalFilesAndInfo(SignalCsvOriginal):
    inputDir = "/home/schnooru/CLIC/Projects/HiggsSelf/CodeRosa/NtupleForMVAProcessor/files_root/anaChain_CLIC_ILD_files_v6/3000GeV"

    couplingsfiles = ReaderTools.walkInputDir(inputDir)

    
    csvfilename = SignalCsvOriginal
    dictCouplings = dictCouplingsCSV(csvfilename)

    #csvfile = open(csvfilename,'rb')
    #newcsvfile = open ("sorted_SignalDSIDs.csv","w")

    #print dictCouplings

    SignalFiles_dict = {}
    
    for ID in dictCouplings.keys():
        found_it, printout = findAnaChainFileInCheckDir( inputDir, ID )
        if found_it:
            DSID_infos = dictCouplings[ID]
            DSID_infos.append(printout)
            SignalFiles_dict[ID] = DSID_infos
    return(SignalFiles_dict)
        #print(printout)

def CreateSignalCSV(csvfilename, SignalInfoDict):
    #print SignalInfoDict

    """ Expects a dictionary of the form 
    <DSID> : [gHHH, gHHWW, xsec, Nsample, filename]
    and returns csv as needed for runOverHH
    """
    

    csvfile = open(csvfilename,"w")
    csvfile.write("#DSID, gHHH, gHHWW, xsec,  Filename, Nsample, comment\n")
    for ID in SignalInfoDict.keys():
        infos = SignalInfoDict[ID]
        #need to swap the NSample and Filename info because it is initially the other way around than expected in the csv
        nsample = infos[3]
        infos[3] = infos[4]
        infos[4]= nsample
        csvfile.write("{}, {}, {}\n".format(str(ID), ", ".join( map(str, infos )),"" ))
    
    return

def NsampleFromAnaChain(SignalInfoDict):
    NewSignalInfoDict = {}
    for DSID in SignalInfoDict.keys():
        signalfilename = SignalInfoDict[DSID][4]
        sigfile = TFile.Open(signalfilename,"rb")
        h = sigfile.Get("h_cutflow")
        nsample = h.GetBinContent(1)
        NewSignalInfoDict[DSID] = [SignalInfoDict[DSID][0], SignalInfoDict[DSID][1],SignalInfoDict[DSID][2],nsample,SignalInfoDict[DSID][4]]
        print ("old {}\nnew {}".format(SignalInfoDict[DSID], NewSignalInfoDict[DSID]))
        sigfile.Close()
    return NewSignalInfoDict

def CreateSignalGroupJson(SignalFilesInfo):
    print SignalFilesInfo
    SigGroupDict = {}

    dictkeys = SignalFilesInfo.keys()
    already_handled = []
    for dsid in dictkeys:
        if dsid in already_handled:
            continue
        SigGroupDict[dsid] = [dsid]
        for o_dsid in dictkeys:
            if dsid == o_dsid:
                continue
            if SameCouplings(SignalFilesInfo, dsid,o_dsid):
                SigGroupDict[dsid].append(o_dsid)
                #record o_dsid such that it is not being treated again
                already_handled.append(o_dsid)
            
    
    with open('SignalGroups.json', 'w') as fp:
        json.dump(SigGroupDict, fp)
    
    return
def SameCouplings(SignalFilesInfo, dsid, o_dsid):
    return    SignalFilesInfo[dsid][0]==SignalFilesInfo[o_dsid][0] and SignalFilesInfo[dsid][1]==SignalFilesInfo[o_dsid][1]

    
def main(*args):
    """ Goes through csv with signal DSIDs and
    creates one including the anachain file paths
    and TODO the correct number of total events

    """
    #go through the csv

    #fill the couplings dictionary
    SignalFilesInfo = FindAllSignalFilesAndInfo("../Configs/SignalsDSIDs_original.csv") 

    
    print("Original table with the numbers from the twiki:\n")
    print (TablePrintout(SignalFilesInfo  ) )
    
    #NEW:
    SignalFilesInfo_Updated = NsampleFromAnaChain(SignalFilesInfo)
    print (TablePrintout(SignalFilesInfo_Updated  ) )


    CreateSignalCSV("newSortedSignalDSIDFiles_v6.csv", SignalFilesInfo_Updated)
    
    CreateSignalGroupJson(SignalFilesInfo_Updated)

    
if __name__ == "__main__":
    main(*sys.argv)
