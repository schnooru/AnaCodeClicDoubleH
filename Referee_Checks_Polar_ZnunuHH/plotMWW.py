"""Reads the truth MC files for the two different polarisations to make correctly normalized plots
   still need to adapt to mWW!

"""
import sys
from ROOT import TFile, gStyle, kOrange, kMagenta, kGreen, kPink, kRed, kBlue, kBlack, kCyan, kAzure, kViolet, TLegend, gPad
from HHTools import dictAnaSample_FromCSV, ReadCsvToDict_floats
from clicpyana.PlottingTools import Plotter


def main(*args):
    #Open the HistoFiles/SignalsSumMj.root
    #extract the relevant histograms from it that correspond to the ones listed in selectedDSTs_Papercomparisons.csv
    #Scale them, then plot them probably using some of the pre-made plotting styles
    gStyle.SetLabelSize(0.04,"xyz")
    gStyle.SetLineWidth(3)

    gStyle.SetOptTitle(0)

    gStyle.SetPadTickX(1)
    gStyle.SetPadTickY(1)

    #create a sample dict from the csv
    samplesDict = {      
        "polp80_WW" :
        {"Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp80/eeWW_polp80.slcio"],
         "cross-section": 209,
         "linecolor" : kCyan,
         "linestyle" : 4,
         "legstring" : "P(e-)=+80%",
         "label": "polp80_WW"},
        "polp85_WW":
        { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp85/eeWW_polp85.slcio"],
          "cross-section": 158,
         "linecolor" : kViolet+2,
         "linestyle" : 2,
          "legstring" : "P(e-)=+85%",
          "label": "polp85_WW" },
        "polp90_WW"  :
        {"Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp90/eeWW_polp90.slcio"],
        "cross-section": 108,
          "linecolor" : kAzure+2,
        "linestyle" : 3,
        "legstring" : "P(e-)=+90%",
       "label": "polp90_WW" },
    "polp95_WW":
    {"Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp95/eeWW_polp95.slcio"],
     "cross-section": 56  ,
          "linecolor" : kPink+2,
     "linestyle" : 5,
     "legstring" : "P(e-)=+95%",
     "label": "polp95_WW"},
    "polp99_WW":
    {"Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp99/eeWW_polp99.slcio"],
     "cross-section": 15  ,
          "linecolor" : kGreen+2,
     "linestyle" : 5,
     "legstring" : "P(e-)=+99%",
     "label": "polp99_WW"},
    "polp100_WW":
    { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp100/eeWW_polp100.slcio"],
      "cross-section": 4 ,
      "linecolor" : kBlue+2,
      "linestyle" : 1,
      "legstring" : "P(e-)=+100%",
      "label": "polp100_WW"
    }
    }
                  
    print samplesDict
    canv = Plotter.createCanvas()
    histlist=[]

    #canv.SetLogy()

    canv.SetLeftMargin(0.15)
    canv.SetBottomMargin(0.18)

 
    DSIDs = ["polp80_WW","polp85_WW","polp90_WW","polp95_WW" ,"polp99_WW","polp100_WW"]


    filedict = {}
    for sample in DSIDs:
        filedict[sample] = TFile.Open("HistoFiles/histofile_{}.root".format(sample))
    
    #signals_linestyle = [4,2,6,3,6]

    leg = TLegend(0.35,0.6,0.87,0.87)
    leg.SetBorderSize(0)
    leg.SetMargin(0.15)
    leg.SetFillStyle(0) #transparent
    
    histoname = "MWW"
    #histoname = "Mqqqq"
    #histoname = "PTW"
    #histoname = "EWW"
    #histoname = "CosThetaWp"
    histoname = "CosThetaWm"



    # DSIDs = ["polp80_WW"]
    # mWW_hist = filedict[DSID].Get("{}_{}".format(histoname , DSID))
    # sigma =   samplesDict[DSID]["cross-section"]
    # mWW_hist.Scale(sigma/mWW_hist.Integral())
    # canv.SetLogy()
    # mWW_hist.Draw()
    # raw_input()
    # return
    
    for DSID in DSIDs:
        print("Getting the histogram from file {}".format(filedict[DSID].GetName()))
        mWW_hist = filedict[DSID].Get("{}_{}".format(histoname , DSID))

        if not mWW_hist:
            print( "histogram for {} not (yet) available (maybe need to hadd the files from fillHistosMWW)".format(DSID))
            continue
        print mWW_hist.Integral()
        sigma =   samplesDict[DSID]["cross-section"]
        mWW_hist.Rebin(10)
        mWW_hist.SetStats(0)
        mWW_hist.Sumw2()
        mWW_hist.SetLineWidth(4)
        mWW_hist.Scale(sigma/mWW_hist.Integral())
        print("{} integral: {}".format(DSID,mWW_hist.Integral()))
        mWW_hist.SetLineColor(samplesDict[DSID]["linecolor"])
        mWW_hist.SetLineStyle(samplesDict[DSID]["linestyle"])
        mWW_hist.Draw("same E1 hist")
        if "CosThetaW" in histoname:
            unit = ""
        else:
            unit = "[GeV]"
        mWW_hist.GetXaxis().SetTitle("{} {}".format(histoname, unit))
        
        xaxis = Plotter.setFeaturesAxis(mWW_hist.GetXaxis(), 1.1,0.06,0.06)
        binwidth = xaxis.GetBinWidth(0)
        #binwidth = int(mWW_hist.GetXaxis().GetBinWidth(0))
        mWW_hist.GetYaxis().SetTitle("d#sigma/d {} [fb/{} {}]".format(histoname,binwidth,unit.replace("[","").replace("]","")))
        #mWW_hist.GetYaxis().SetTitle("1/#sigma d#sigma/d {}".format(histoname,binwidth,unit.replace("[","").replace("]","")))
        #mWW_hist.GetYaxis().SetTitle("#sigma [fb]")
        yaxis = Plotter.setFeaturesAxis(mWW_hist.GetYaxis(), 1.4,0.06,0.06)
        #mWW_hist.GetYaxis().SetRangeUser(0.01, 0.5)
        #canv.SetLogy()

        mWW_hist.GetXaxis().SetRangeUser(-0.99, 0.99)
        mWW_hist.GetYaxis().SetRangeUser(0.01, 200)


        mWW_hist.GetXaxis().SetRangeUser(-0.99, 0.99)
        mWW_hist.GetYaxis().SetRangeUser(0.01, 200)
        for i in range(0,mWW_hist.GetNbinsX()+2):
            print mWW_hist.GetBinContent(i)
    

        leg.AddEntry(mWW_hist,"{} ".format(samplesDict[DSID]["legstring"])  ,"l")
        histlist.append(mWW_hist)
    #raw_input()

            
    # Plotter.plotComparison(canv,histlist,maxfact=1.4,legendymin=0.7,textsize=0.05)
    leg.Draw()
    canv.SetLeftMargin(0.18)
    canv.SetLogy()
    
    canv.Modified()
    canv.Update()

    raw_input()
    canv.SaveAs("{}_truthSignal_with_{}.pdf".format(histoname,"_".join(map(str,DSIDs))))
    
    for f in filedict:
        filedict[f].Close()

    
    return


if __name__ == "__main__":
    main(*sys.argv)