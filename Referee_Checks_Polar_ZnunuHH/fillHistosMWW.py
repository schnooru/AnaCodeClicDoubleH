"""
still need to adapt to MWW!
"""
#!/cvmfs/clicdp.cern.ch/DIRAC/Linux_x86_64_glibc-2.12/bin/python
from pyLCIO import IOIMPL
import sys
from ROOT import TCanvas, TH1D, TF1, TLorentzVector, TVector3, TFile
from math import cos
    
def FillHistosFromEvents(prodid, reader ):

    # create a histogram for the hit energies
    MWWtruthHistogram = TH1D( "MWW_{}".format(prodid), 'MWW truth;MWW truth[GeV];Entries', 100, 0., 3000. )
    MWWHistogram = TH1D( "Mqqqq_{}".format(prodid), 'M(qqqq) parton;M(qqqq) parton[GeV];Entries', 100, 0., 3000. )
    PTWHistogram = TH1D( "PTW_{}".format(prodid), 'P_T(W) truth; P_T(W) [GeV];Entries', 100,0.,1500 )
    SumEHistogram = TH1D( "EWW_{}".format(prodid), 'EWW truth;EWW truth[GeV];Entries', 100, 0., 3000. )
    CosThetaWHistogram = TH1D( "CosThetaW_{}".format(prodid), 'cos ThetaW truth;cos(#theta)_{W} truth;Entries', 100, -1., 1. )
    CosThetaWmHistogram = TH1D( "CosThetaWm_{}".format(prodid), 'cos ThetaWm truth;cos(#theta)_{W-} truth;Entries', 100, -1., 1. )
    CosThetaWpHistogram = TH1D( "CosThetaWp_{}".format(prodid), 'cos ThetaWp truth;cos(#theta)_{W+} truth;Entries', 100, -1., 1. )
    nevent = 0
    n_lastbin = 0
    # loop over all events in the file
    for event in reader:
        nevent += 1
        # get the collection from the event

        truthparticleCollection = event.getCollection("MCParticle")
        quark_sum = TLorentzVector()
        W_sum = TLorentzVector()
        #W1, W2 =  TLorentzVector(),  TLorentzVector()
        #might need the Ws
        nquark = 0
        nW = 0   
        W_energysum = 0
        for particle in truthparticleCollection:
            PDGID = particle.getPDG()
            #only the first 4 quarks!
            if abs(PDGID) <= 6 and nquark <=4:
                nquark += 1
                quark_sum += particle.getLorentzVec()
            #use only events with 2 Ws (qqqq can also be oter things)

            if abs(PDGID) == 24:
                nW += 1

        if nW != 2:
            continue

        for particle in truthparticleCollection:
            PDGID = particle.getPDG()
            if PDGID == 24:
                CosThetaWp = cos(particle.getLorentzVec().Theta())
            if PDGID == -24:
                #if cos(particle.getLorentzVec().Theta()) < 0.8:
                CosThetaWm = cos(particle.getLorentzVec().Theta())
            if abs(PDGID) == 24:
                W_sum += particle.getLorentzVec()
                W_energysum += particle.getLorentzVec().E()
                PTW = particle.getLorentzVec().Pt() 
                CosThetaWHistogram.Fill(cos(particle.getLorentzVec().Theta()))
            #if PDGID == -11: #electron
                #print(dir(particle))
                #print(particle.getSpin())

        SumEHistogram.Fill( W_sum.E() )
        MWWtruthHistogram.Fill(W_sum.M())

        #if CosThetaWp < -0.8: continue
        #if CosThetaWm > 0.8: continue
        #if PTW < 600 : continue
        PTWHistogram.Fill(PTW)
        CosThetaWpHistogram.Fill(CosThetaWp)
        CosThetaWmHistogram.Fill(CosThetaWm)
            
        MWWHistogram.Fill(quark_sum.M())
        if CosThetaWm > 0.95: n_lastbin +=1
    reader.close()

    fraction_lastbin = float(n_lastbin)/nevent
    print("Fraction in the last bin costhetaW- > 0.9: {}".format(fraction_lastbin))
    return nevent , [MWWHistogram,
                     SumEHistogram,
                     MWWtruthHistogram,
                     PTWHistogram,
                     CosThetaWHistogram,
                     CosThetaWpHistogram,
                     CosThetaWmHistogram]

def FileNumberFromName(filename):
    print filename
    return


def GetHistogramsForInputfile( inputfilename, outputfile, label):
    if not label in outputfile.GetName():
        print("WARNING: The label and the name of the outputfile do not seem to match")
    print( "Reading input file {}".format( inputfilename ) )
    print("Writing to {}".format(outputfile.GetName()))
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    reader.open(inputfilename)
    nevents, histos = FillHistosFromEvents(label, reader)
    for histo in histos:
        histo.Write()
                #can I just keep it open for each input file and keep writing/Adding?
    return outputfile

def GetHistogramsForInputfileList( inputfilelist, outfile, label): 
    print("WARNING have not tested GetHistogramsForInputfileList yet")
    #same but with a list, but writing only to the same outfile, maybe use Add? (Write probably overwrites instead of adding the histograms)
    reader = IOIMPL.LCFactory.getInstance().createLCReader()
    for inputfilename in inputfilelist:
        reader.open(inputfilename)
        nevents, histos = FillHistosFromEvents(label, reader)
        for histo in histos:
            if outfile.Get(histo.GetName()): #basically, it already exists! maybe need a try statement
                orighist = outfile.Get(histo.GetName())
                histo = orighist.Add(histo)
            histo.Write()

    return outfile

def main(*args):
    
    #supply:
    #input file [list] together with a label (polm80_WW or polp100_qqqq or so) 


    #inputdict = { "Filelist" : [ /home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/polp80/eeWW_polp80.slcio ] , "label" : "polp80_WW", "cross-section" : , }

    inputdict = { "Samplenamesfile" : "/eos/home-s/schnooru/Data_CLIC/WW_polarisation/fileslocal_polm80.list",
                  "label": "polm80_qqqq",
                  "cross-section": 902    }

    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/MonteCarloAtCLIC/2019-02_qqqq/polarisation/polm100/polm100_qqqq.slcio"],
                  "cross-section" : 9.9737387E+02 ,
                  "label": "polm100_qqqq" }

    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/MonteCarloAtCLIC/2019-02_qqqq/polarisation/polp100/polp100_qqqq.slcio"],
                   "cross-section" : 2.2891311E+01,
                  "label": "polp100_qqqq"}


    
    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polm80/eeWW_polm80.slcio"],
                  "cross-section": 1851,
                  "label": "polm80_WW"}
    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polm85/eeWW_polm85.slcio"],
                  "cross-section": 1904,
                  "label": "polm85_WW" }
    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polm90/eeWW_polm90.slcio"],
                  "cross-section": 1955,
                  "label": "polm90_WW" }
    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polm95/eeWW_polm95.slcio"],
                  "cross-section": 2017,
                  "label": "polm95_WW"}

    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polm100/eeWW_polm100.slcio"],
                  "cross-section": 2066,
                  "label": "polm100_WW"
              }

    
    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp80/eeWW_polp80.slcio"],
                  "cross-section": 209,
                  "label": "polp80_WW"}
    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp85/eeWW_polp85.slcio"],
                  "cross-section": 158,
                  "label": "polp85_WW" }
    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp90/eeWW_polp90.slcio"],
                  "cross-section": 108,
                  "label": "polp90_WW" }
    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp95/eeWW_polp95.slcio"],
                  "cross-section": 56  ,
                  "label": "polp95_WW"}

    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp100/eeWW_polp100.slcio"],
                  "cross-section": 4 ,
                  "label": "polp100_WW"
              }
    inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/polp99/eeWW_polp99.slcio"],
                  "cross-section": 15 ,
                  "label": "polp99_WW"
              }



    # inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/epolm80_ppolp100/eeWW_epolm80_ppolp100.slcio"],
    #               "cross-section" : 3699,
    #               "label": "epolm80_ppolp100_WW" }

    # inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/epolm90_ppolp100/eeWW_epolm90_ppolp100.slcio"],
    #               "cross-section" : 3911,
    #               "label": "epolm90_ppolp100_WW" }

    # inputdict = { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/Polarisation_WW/eeWWfiles/linearity/epolm100_ppolp100/eeWW_epolm100_ppolp100.slcio"],
    #               "cross-section" : 4118,
    #               "label": "epolm100_ppolp100_WW" }



    
                  #starting for one outputfile
                  
    outfilename = "HistoFiles/histofile_{}.root".format( inputdict["label"] )
    outfile = TFile.Open(outfilename, "RECREATE")
    outfile = GetHistogramsForInputfile( inputdict["Filelist"][0], outfile, inputdict["label"])
    outfile.Close()
    # # create a reader and open an LCIO file
    # reader = IOIMPL.LCFactory.getInstance().createLCReader()
    # #prodids = ["polm80","polp80"]
   
    
    # #    prodids = ["polm80_WW","polp80_WW"]

    # for i in prodids:
    #     samplenamesfile = "/eos/home-s/schnooru/Data_CLIC/WW_polarisation/fileslocal_{}.list".format(i)
    #     #samplenamesfile = "/home/schnooru/CLIC/Projects/HiggsSelf//Polarisation_WW/eeWWfiles/files_{}.list".format(i)
        
    #     f = open(samplenamesfile,'r')
    #     nfile = 0
    #     for line in f:
    #         nfile+=1
    #         print("Opening file {}".format(line.strip("\n")))
    #         reader.open(line.strip("\n").strip("\n"))
    #         outfile = "HistoFiles/histofile_SumMW_{}_{}.root".format(i,nfile)
    #         #outfile = "HistoFiles/histos_eeWW_{}_{}.root".format(i,nfile)

    #         hfile = TFile.Open(outfile,"RECREATE")
    #         nevents, histos = FillHistosFromEvents(i, reader)
    #         for histo in histos:
    #             histo.Write()
    #             #can I just keep it open for each input file and keep writing/Adding?
    #         print("Writing histogram to {}".format(outfile))
    #         hfile.Close()
    #     f.close()

    return




if __name__ == "__main__":
    main(*sys.argv)
