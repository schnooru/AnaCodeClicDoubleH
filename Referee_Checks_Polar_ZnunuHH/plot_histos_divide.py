"""Reads the truth MC files for the two different polarisations to make correctly normalized plots
   still need to adapt to mWW!

"""
import sys
from ROOT import TFile, gStyle, kOrange, kMagenta, kGreen, kPink, kRed, kBlue, kBlack, kCyan, kAzure, kViolet, TLegend, gPad
from HHTools import dictAnaSample_FromCSV, ReadCsvToDict_floats
from clicpyana.PlottingTools import Plotter


def main(*args):
    #Open the HistoFiles/SignalsSumMj.root
    #extract the relevant histograms from it that correspond to the ones listed in selectedDSTs_Papercomparisons.csv
    #Scale them, then plot them probably using some of the pre-made plotting styles
    gStyle.SetLabelSize(0.04,"xyz")
    gStyle.SetLineWidth(3)

    gStyle.SetOptTitle(0)

    gStyle.SetPadTickX(1)
    gStyle.SetPadTickY(1)

    #create a sample dict from the csv
    samplesDict = {      
     

    "nocuts":
    {"Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/AnaCodeClicDoubleH/DoubleH/HistoFiles_28-05-20_B5/Sig_nocuts_2018_withabscostheta.root"],
     "cross-section": 1,
          "linecolor" : kGreen+2,
     "linestyle" : 5,
     "legstring" : "no cuts",
     "label": "nocuts"},
    "selection":
    { "Filelist" : ["/home/schnooru/CLIC/Projects/HiggsSelf/AnaCodeClicDoubleH/DoubleH/HistoFiles_28-05-20_B5/Sig_selection_2018_withabscostheta.root"],
      "cross-section": 1,
      "linecolor" : kBlue+2,
      "linestyle" : 1,
      "legstring" : "tight BDT",
      "label": "tightBDT"
    }
    }
                  
    print samplesDict
    canv = Plotter.createCanvas()
    histlist=[]

    #canv.SetLogy()

    canv.SetLeftMargin(0.15)
    canv.SetBottomMargin(0.18)

 
    DSIDs = ["nocuts","selection"]


    filedict = {}
    for sample in DSIDs:
        filedict[sample] = TFile.Open("../DoubleH/HistoFiles_28-05-20_B5/Sig_{}_2018_withabscostheta.root".format(sample))
    
    #signals_linestyle = [4,2,6,3,6]

    leg = TLegend(0.35,0.7,0.87,0.87)
    leg.SetBorderSize(0)
    leg.SetMargin(0.15)
    leg.SetFillStyle(0) #transparent
    


    histoname = "SumMj"
    histoname = "abs_cos_thetaH"
    histoname = "SumMj"


    nocuts_hist = filedict["nocuts"].Get("{}_7181".format(histoname ))
    selec_hist  = filedict["selection"].Get("{}_7181".format(histoname ))

    nocuts_hist.Rebin(10)
    selec_hist .Rebin(10)
    nocuts_hist.Sumw2()
    selec_hist .Sumw2()
    nocuts_hist.SetLineWidth(4)
    selec_hist .SetLineWidth(4)
    selec_hist.SetStats(0)

    selec_hist.Divide(nocuts_hist)
    selec_hist.GetYaxis().SetTitle("Efficiency")

    selec_hist.GetYaxis().SetRangeUser(0,0.3)
    selec_hist.GetYaxis().SetTitleOffset(1.3)
    selec_hist.Draw()
    leg.AddEntry(selec_hist,"#frac{N(tightBDT)}{N(no cuts)}"  ,"l")
    leg.Draw()
    canv.SetLeftMargin(0.18)
    
    canv.Modified()
    canv.Update()
    save_it = raw_input("Save or not? [y/N]")
    if save_it == "y":
        canv.SaveAs("Efficiency_{}_Signal.pdf".format(histoname))
    
    raw_input("Continuing to the histograms normalized to 1")

    
    for DSID in DSIDs:
        print("Getting the histogram from file {}".format(filedict[DSID].GetName()))
        mWW_hist = filedict[DSID].Get("{}_7181".format(histoname ))

        if not mWW_hist:
            print( "histogram for {} not (yet) available (maybe need to hadd the files from fillHistosMWW)".format(DSID))
            continue
        print mWW_hist.Integral()
        sigma =   samplesDict[DSID]["cross-section"]
        mWW_hist.Rebin(10)
        mWW_hist.SetStats(0)
        mWW_hist.Sumw2()
        mWW_hist.SetLineWidth(4)
        mWW_hist.Scale(sigma/mWW_hist.Integral())
        print("{} integral: {}".format(DSID,mWW_hist.Integral()))
        mWW_hist.SetLineColor(samplesDict[DSID]["linecolor"])
        mWW_hist.SetLineStyle(samplesDict[DSID]["linestyle"])
        mWW_hist.Draw("same E1 hist")
        if "abs_cos_thetaH" in histoname:
            unit = ""
        else:
            unit = "[GeV]"
        histoname_nice = histoname
        if histoname == "SumMj": histoname_nice = "M(HH)"
        elif histoname == "abs_cos_thetaH": histoname_nice = "|cos(#theta_{H})|"
        mWW_hist.GetXaxis().SetTitle("{} {}".format(histoname_nice, unit))
        

        leg.AddEntry(mWW_hist,"{} ".format(samplesDict[DSID]["legstring"])  ,"l")
        histlist.append(mWW_hist)

    # Plotter.plotComparison(canv,histlist,maxfact=1.4,legendymin=0.7,textsize=0.05)
    leg.Draw()
    canv.SetLeftMargin(0.18)
    #canv.SetLogy()
    
    canv.Modified()
    canv.Update()

    save_it = raw_input("Save or not? [y/N]")
    if save_it == "y":
        canv.SaveAs("{}_Signal_with_{}.pdf".format(histoname,"_".join(map(str,DSIDs))))
    
    for f in filedict:
        filedict[f].Close()

    
    return


if __name__ == "__main__":
    main(*sys.argv)
