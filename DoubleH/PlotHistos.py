from clicpyana.PlottingTools import Plotter
from ROOT import gStyle, gPad, TText, kBlack,kWhite, TLine, kRed, kMagenta, kBlue, TGaxis, TPad, gEnv, TLatex

class PlotHistos:
    def __init__(self,  runConfig, histoConfigAll1D={}):

        self.histoConfigAll1D = histoConfigAll1D
        if len(self.histoConfigAll1D) ==0:
            self.histoConfigAll1D = runConfig["Run_Histo_Config"]
        return

    def SetGeneralStyle(self,canv,histo,histoConfig, WIP=False,nolumi=False, Is2D=False,isWideCanvas=False):
        canv.cd()
        Title = histoConfig["Title"]
        xTitle = histoConfig["xTitle"]
        yTitle = histoConfig["yTitle"]
        lumi = histoConfig["lumi"]
        gStyle.SetPalette(56) #105, 107, 57
        gStyle.SetNumberContours(50)
        gStyle.SetPadTickY(1)
        gStyle.SetPadTickX(1)

        try:
            histo.SetStats(0)
        except AttributeError:
            pass
        gPad.SetTicks(1,1)
        canv.SetBottomMargin(0.17)
        canv.SetLeftMargin(0.2)
        #histo.Draw("")
        histo.SetTitle(Title)
        histo.GetXaxis().SetTitle(xTitle)
        histo.GetXaxis().SetTitleSize(1.6*histo.GetXaxis().GetTitleSize())
        histo.GetYaxis().SetTitleSize(1.2*histo.GetXaxis().GetTitleSize())
        histo.GetYaxis().SetTitleOffset(1.6*histo.GetYaxis().GetTitleOffset())
        histo.GetXaxis().SetLabelSize(0.06)
        histo.GetYaxis().SetLabelSize(0.06)
        histo.GetYaxis().SetTitle(yTitle)
        histo.GetXaxis().SetNdivisions(505)

        clictext = "CLICdp"
        if WIP:
            clictext += " Work in Progress"
        t1 = TText(0.5,0.5,clictext)
        t1.SetTextSize(0.05)
        t1.SetTextColor(kBlack)
        t1.SetTextFont(42)


        lumitext = "L={}/fb".format(lumi)
        cmetext = "3 TeV"
        
        lumit=TText(0.5,0.5,lumitext)
        lumit.SetTextSize(0.05)
        lumit.SetTextColor(kBlack)
        lumit.SetTextFont(42)
        printpol = False
        if "polarization" in histoConfig.keys():
            printpol = True
        if printpol:
            poltext = self.CreatePolarizationLabel( histoConfig, isWideCanvas)
            polt=TText(0.5,0.5,poltext)
            polt.SetTextSize(0.05)
            polt.SetTextColor(kBlack)
            polt.SetTextFont(42)

        x_t1 = 0.2
        y_t1 = 0.92
        x_l1 = 0.24
        y_l1 = 0.84
        x_p1 = 0.56
        y_p1 = 0.84
        if Is2D and WIP:
            x_t1 = 0.21
            y_t1 = 0.84
            x_l1 = 0.7
            y_l1 = 0.84
            x_p1 = 0.21
            y_p1 = 0.78
        elif isWideCanvas:
            x_t1 = 0.12
            y_t1 = 0.92
            x_l1 = 0.56
            y_l1 = 0.92
            x_p1 = 0.715
            y_p1 = 0.92

        t1.DrawTextNDC(x_t1,y_t1, clictext)
        if not nolumi:
            lumit.DrawTextNDC(x_l1,y_l1,lumitext)
        if printpol and not nolumi:
            polt.DrawTextNDC(x_p1,y_p1,poltext)
        lumit.DrawTextNDC(x_l1,y_l1-0.06, cmetext)
        canv.Modified()
        canv.Update()
        return canv, histo, t1, lumit


    def SetGeneralStyleChi2(self,canv,histo,histoConfig,isWithZHH, extratext = "", nolumi=False, Is2D=False,isWideCanvas=False):
        canv.cd()
        Title = histoConfig["Title"]
        xTitle = histoConfig["xTitle"]
        yTitle = histoConfig["yTitle"]
        lumi = histoConfig["lumi"]
        gStyle.SetPalette(56) #105, 107, 57
        gStyle.SetNumberContours(50)
        gStyle.SetPadTickY(1)
        gStyle.SetPadTickX(1)

        try:
            histo.SetStats(0)
        except AttributeError:
            pass
        gPad.SetTicks(1,1)
        canv.SetBottomMargin(0.17)
        canv.SetLeftMargin(0.2)
        #histo.Draw("")
        histo.SetTitle(Title)
        histo.GetXaxis().SetTitle(xTitle)
        histo.GetXaxis().SetTitleSize(1.6*histo.GetXaxis().GetTitleSize())
        histo.GetYaxis().SetTitleSize(1.2*histo.GetXaxis().GetTitleSize())
        histo.GetYaxis().SetTitleOffset(1.6*histo.GetYaxis().GetTitleOffset())
        histo.GetXaxis().SetLabelSize(0.06)
        histo.GetYaxis().SetLabelSize(0.06)
        histo.GetYaxis().SetTitle(yTitle)
        histo.GetXaxis().SetNdivisions(505)

        clictext = "CLICdp"

        t1 = TText(0.5,0.5,clictext)
        t1.SetTextSize(0.05)
        t1.SetTextColor(kBlack)
        t1.SetTextFont(42)
        t1.DrawTextNDC(0.2,0.92, clictext)
        #create the polarisation scheme label
        t1.DrawTextNDC(0.6,0.92, "4:1 pol. scheme")

        pollab=[]
        #create a label for 3TeV HHvv
        pollab_0 = self.CreateSmallText(0.05)
        pollab_0.DrawLatex(0.6,26,"HH#nu#nu: 3 TeV; 5 ab^{-1}")
        pollab.append(pollab_0)

        #create a label for 1.4TeV ZHH
        pollab_0 = self.CreateSmallText(0.05)
        if isWithZHH:
            pollab_0.DrawLatex(1.5,26,"+ ZHH: 1.4 TeV; 2.5 ab^{-1}")
        pollab.append(pollab_0)
        
        #create a "rate only" or "differential" label:
        pollab_0 = self.CreateSmallText(0.05)
        pollab_0.DrawLatex(0.7,23,extratext)
        pollab.append(pollab_0)
        
     
        return canv, histo, t1, pollab

        
    def CreatePolarizationLabel(self, histoConfig,isWideCanvas):
        if histoConfig["polarization"] == 1.0:
            poltext = "no pol."
        elif histoConfig["polarization"] == 1.48:
            poltext = "4:1 pol. scheme"
        elif histoConfig["polarization"] == 1.8:
            poltext = "full -80% e^- pol."
       

        return poltext
    def Plot1D(self,graph,histoConfig,WIP=True):
        canv = Plotter.createCanvas()

        graph.Draw("AP")
        graph.SetMarkerColor(kBlue+2)
        graph.SetMarkerStyle(20)#(21)
        graph.SetMarkerSize(0.8)
#        graph.GetXaxis().SetTitleSize(0.08)
#        graph.GetYaxis().SetTitleSize(0.08)
#        graph.GetYaxis().SetTitleOffset(1.2)
#        graph.GetXaxis().SetLabelSize(0.08)
#        graph.GetYaxis().SetLabelSize(0.08)

        canv, graph,t1,lumit = self.SetGeneralStyle(canv,graph,histoConfig,WIP=WIP,nolumi=True,Is2D=True)

        canv.SetGridy()
        canv.SetLeftMargin(0.2)
        canv.SetBottomMargin(0.2)
        graph.SetTitle("")
        canv.Modified()
        canv.Update()
        yaxis = graph.GetYaxis()

        line =  TLine()
        line.SetLineColor(kMagenta+2)
        line.SetLineWidth(3)
        line.SetLineStyle(9)
        line.DrawLine(1,0.4,1,0.85)
        liney =  TLine()
        liney.SetLineStyle(9)
        liney.SetLineColor(kMagenta+2)
        liney.SetLineWidth(3)
        liney.DrawLine(0,0.589,2.8,0.589)
        line2 =  TLine()
        line2.SetLineStyle(9)
        line2.SetLineColor(kMagenta+2)
        line2.SetLineWidth(3)
        line2.DrawLine(2.28,0.4,2.28,0.85)
        
        canv.Modified()
        canv.Update()

        return canv, graph

    def Plot2D(self,hist2D, histoConfig,isWithZHH):
        print("Creating Plot for histogram {} with title {}".format( hist2D.GetName(), histoConfig["Title"]  ))
        canv = Plotter.createCanvas()

        #hist2D.Draw("colz")
        #hist2D.Draw("colz")
        hist2D.Draw("colz")
        xTitle = histoConfig["xTitle"]
        yTitle = histoConfig["yTitle"]
        gStyle.SetNumberContours(150)

        noWriteLumi =  histoConfig["lumi"]==0


        canv, hist2D,t1,lumit = self.SetGeneralStyleChi2(canv,hist2D,histoConfig,isWithZHH, extratext = "differential", nolumi=False, Is2D=False,isWideCanvas=False)
#
        xaxis = hist2D.GetXaxis()
        xaxis.SetTitle(xTitle)

        hist2D.GetXaxis().SetTitleSize(0.06)
        hist2D.GetYaxis().SetTitleSize(0.06)
        hist2D.GetXaxis().SetLabelSize(0.06)
        hist2D.GetYaxis().SetLabelSize(0.06)
        yaxis = hist2D.GetYaxis()
        yaxis.SetTitle(yTitle)

        if "xlimits" in histoConfig:
            xaxis.SetRangeUser( histoConfig["xlimits"][0],histoConfig["xlimits"][1])
        if "ylimits" in histoConfig:
            yaxis.SetRangeUser( histoConfig["ylimits"][0],histoConfig["ylimits"][1])
            
        
        return canv, hist2D

    def PrepareCanvas(self,wideCanvas=False):
        listofpads = []
        if wideCanvas:
            gStyle.SetLegendTextSize(0.09)
            canv = Plotter.createCanvas(canv_name="canv",xwidth=1000,ywidth=500)
            pad_plot = TPad("plotpad", "plotpad",0.0,0.,0.75,1.)
            pad_plot.SetBorderSize(0)
            pad_plot.Draw()
            listofpads.append(pad_plot)
            pad_legend = TPad("legendpad","legendpad",0.75,0.,1.,1.)
            pad_legend.SetBorderSize(0)
            pad_legend.Draw()
            listofpads.append(pad_legend)
            pad_plot.cd()
            pad_plot.SetRightMargin(0.02)
            
            
            #then, make 2 pads and cd to the left one.
            #then cd to the right one for legend plotting only if wideCanvas.
        else:
            gStyle.SetLegendTextSize(0.04)
            canv = Plotter.createCanvas()
        return canv, listofpads

    def PlotFromOneStack( self,hist1Dstack,  legend , histoConfig, wideCanvas=False):
        canv,pads = self.PrepareCanvas(wideCanvas)
        if len(pads)>0:
            pads[0].cd()
        hist1Dstack.Draw("HIST")
        canv.Modified()
        canv.Update()
        h0= hist1Dstack.GetHists()[0]
        #print("PlotHistos: Original bin label of first bin: {}".format( h0.GetXaxis().GetBinLabel(2) ))
        xlow = h0.GetXaxis().GetBinLowEdge( h0.GetXaxis().GetFirst() )
        xup =  h0.GetXaxis().GetBinUpEdge( h0.GetXaxis().GetLast() )

        hist1Dstack.GetXaxis().SetRangeUser(xlow,xup)

        
        if wideCanvas:
            pads[0], hist1Dstack, t1,lumit = self.SetGeneralStyle(pads[0],hist1Dstack, histoConfig,True,False,False,wideCanvas)
        else:
            canv, hist1Dstack, t1,lumit = self.SetGeneralStyle(canv,hist1Dstack, histoConfig,True,False,False,wideCanvas)
       

        canv.Modified()
        canv.Update()

        if len(pads)>1:
            pads[1].cd()
            legend.SetX1(0.0)
            legend.SetX2(1)
            legend.SetY1(0.0)
            legend.SetY2(1)
            legend.SetBorderSize(0)
        legend.Draw()
        canv.Modified()
        canv.Update()

        
        return canv, hist1Dstack, legend, pads
    def PlotFromStacks(self, stacklist, legend , histoConfig, wideCanvas=False, ymax=200):
        canv,pads = self.PrepareCanvas(wideCanvas)
        if len(pads)>0:
            pads[0].cd()

        stacklist[0].Draw("HIST")

        stacklist[0].SetMaximum(ymax)

        if len(stacklist)>1:
            for h in stacklist[1:]:
                h.Draw("HIST same")

        if len(pads)>1:
            pads[1].cd()
            legend.SetX1(0.0)
            legend.SetX2(1)
            legend.SetY1(0.0)
            legend.SetY2(1)
            legend.SetBorderSize(0)

        legend.SetFillStyle(0)
        legend.Draw()
        if wideCanvas:
            pads[0], stacklist[0],t1,lumit = self.SetGeneralStyle(pads[0],stacklist[0], histoConfig,True,False,False,wideCanvas)
        else:
            canv, stacklist[0],t1,lumit = self.SetGeneralStyle(canv,stacklist[0], histoConfig,True,False,False,wideCanvas)
        
        canv.Modified()
        canv.Update()

        
        return canv, stacklist, legend,pads


    def TreatKinematic2Dplots(self,canv,histos,ymax,pads):
        gStyle.SetPadTickY(0)
        print("treating the formerly 2D histogram")
        pads[0].SetBottomMargin(0.38)
        pads[0].SetLeftMargin(0.12)
        pads[0].SetRightMargin(0.08)

        histos.GetYaxis().SetTitleOffset(0.90)
        #print ymax
        yaxis_orig = histos.GetYaxis()
        xaxis_orig = histos.GetXaxis()
        old_title_y = yaxis_orig.GetTitle()
        yaxis_orig.SetTitle("N_{events}")
        # for the bin labels!
        #create a new axis object with the same properties


        ymin = 0
        #based on the Histogram.json or based on bin labels?
        nbins0=0
        nbins1=0
        labels =[]
        for b in xrange(1,xaxis_orig.GetLast()+1):
            labels.append(xaxis_orig.GetBinLabel(b).split("&"))
            #print xaxis_orig.GetBinLabel(b).split("&")
        for a in labels:
            if labels[0][0]==a[0]:
                nbins0+=1
            if labels[0][1]==a[1]:
                nbins1+=1
        #now I know that after each nbins1, the axis should come
        xpositions =  [ x*(nbins1) for x in range(nbins0)]
        ndivisions =yaxis_orig.GetNdivisions()
        axes = []
        for xposition in xpositions:
            axis = self.CreateAdditionalAxis(xposition,ymin,ymax,ndivisions)
            axis.Draw()
            axes.append(axis)


        #now, treat the xaxis and correct the labels on it
        bins1labels=[]
        #go through all the bins
        nbin=0
        for b in xrange(1,xaxis_orig.GetLast()+1):
            #print labels[nbin]
            if nbin%nbins1 ==0:
                bins1labels.append(labels[nbin][1])
            #histos.GetXaxis().ChangeLabel(nbin+1,-1,-1., -1,-1,-1, labels[nbin][0])
            #histos.GetXaxis().SetBinLabel(nbin+1, labels[nbin][0])
            ########print histos.GetXaxis().GetBinLabel(nbin+1)
            nbin+=1
        xaxis_orig.SetTitleOffset(2.1)
        if old_title_y == "BDT":
            xaxis_orig.SetTitleOffset(2.1)
        if histos.GetXaxis().GetTitle() == "SumMj":
            histos.GetXaxis().SetTitle("M(HH)[GeV]")
            pads[0].SetBottomMargin(0.22)
        histos.GetXaxis().SetLabelSize(0)
        histos.GetXaxis().SetLabelColor(0)
        
        ndivisions = histos.GetXaxis().GetNdivisions()

        xaxis_new = TGaxis(0,0, nbins1*nbins0,12,0,nbins1*nbins0,121,"+")
        xaxis_new.SetLabelFont(42)
        if old_title_y == "BDT":
            xaxis_new.SetLabelSize(0.05)
            histos.GetYaxis().SetLabelSize(0.05)
        xaxis_new.SetLabelColor(1)
        angle = 90
        if old_title_y == "BDT":
            angle = 0
        angle=40
        for b in xrange(1,nbins1*nbins0+1 ):
            #print (b, labels[b-1][0])
            #newlabel = self.AdjustXbinlabel(labels[b-1][0].split("-")[0])

            newlabel = labels[b-1][0].replace("-","...")
            if old_title_y == "BDT":
                newlabel = newlabel.replace(".0","")
                #print("====bin {}, label {}".format(b,newlabel))
            #if b==1 and old_title_y == "BDT":
            #    newlabel = " "
            xaxis_new.ChangeLabel(b,angle,-1., 0,-1,-1,newlabel ) #this is the part that changes the labels
            #xaxis_new.CenterLabels()
            ###print(b,labels[b-1][0])
        lastbinup = "0.25"
        if old_title_y == "BDT":
            lastbinup = "  3000"
            lastbinup = "  "
        xaxis_new.ChangeLabel(nbins1*nbins0+1,angle,-1., 0,-1,-1,lastbinup ) #this is the part that changes the last label
        xaxis_new.SetName("")
#        xaxis_new.CenterLabels()
        xaxis_new.SetLabelOffset(-0.07)
        if old_title_y == "BDT":
            xaxis_new.SetLabelOffset(-0.09)
            
        xaxis_new.Draw()
        pads[0].Modified()
        pads[0].Update()
        xaxis_new.Draw()

        #then, draw the small text pads with the "y" binnings
        #use old_title_y
        y_variable = old_title_y
        if old_title_y == "SumMj":
            y_variable = "#frac{M(HH)}{GeV}"
        print("there will be n={} bin labels for the yvariable in the end, namely {}".format(nbins0, bins1labels))

        increment=nbins1#1./(nbins1)
        xlocation=0.3
        for i in xrange(nbins0):
            bintext = bins1labels[i]
            bintext= self.AdjustYbinlabel(bintext,y_variable)
            binlabeltext = self.CreateSmallText()
            #binlabeltext.DrawTextNDC(xlocation,0.65,bintext)
            binlabeltext.SetTextSize(0.048)
            binlabeltext.DrawLatex(xlocation,ymax*0.9,bintext)
            xlocation+=increment

        return histos, axes, canv, xaxis_new

    def AdjustXbinlabel(self,label):
        labelparts = label.split(".")
        if len(labelparts) == 2 and labelparts[1] == "0":
            label = labelparts[0] 
            if not label == "0":
                label+= " "
        return label

    def AdjustYbinlabel(self,label,variablename):
        if label == "0.0-400.0":
            return "  "+ variablename + " < 400"
        if not variablename == "BDT":
            label =label.replace(".0","")
        newlabel = label.split("-")[0]+ " < "+variablename+ " < "+ label.split("-")[1]
        return newlabel

    def CreateSmallText(self,size=0.038):

        #TLatex latex;
        #latex.SetTextSize(0.025);
        #latex.SetTextAlign(13);  //align at top
        #latex.DrawLatex(.2,.9,"K_{S}");
        #latex.DrawLatex(.3,.9,"K^{*0}");
        #latex.DrawLatex(.2,.8,longstring);
        #smallt=TText(xposition,yposition,smalltext)
        #smallt.SetTextSize(0.04)
        #smallt.SetTextFont(42)
        #smallt.SetTextColor(kBlack)

        smallt = TLatex()
        smallt.SetTextSize(size)
        smallt.SetTextFont(42)
        smallt.SetTextColor(kBlack)
        
        return smallt

    def CreateAdditionalAxis(self,xposition,ymin,ymax,ndivisions):
        ymax = 1.05*ymax #arbitrary but seems to work...
        axis = TGaxis(xposition,ymin,xposition,ymax,ymin,ymax,ndivisions,"U-" )
        axis.SetName("")
        print(xposition,ymin,xposition,ymax,ymin,ymax)
        print("Creating an additional axis with ymax = {}".format(ymax))

        axis.Draw()
        return axis
