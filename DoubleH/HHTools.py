import csv

def dictAnaSample_FromCSV(csvfilename):
    dictAnaSample = {}
    csvfile = open(csvfilename,'rb')
    reader = csv.reader(csvfile)
    for line in reader:
        if line[0].startswith("#"): continue
        if len(line) < 5:
            print "Not sufficient info for {0}".format(line)
            continue

 
        dsid = line[0].strip()
        dictAnaSample[dsid] ={}
        dictAnaSample[dsid]["couplings"] = [float(line[1].strip()), float(line[2].strip())]
        dictAnaSample[dsid]["xsec"] = float(line[3])
        dictAnaSample[dsid]["path"] = line[4].strip()
        dictAnaSample[dsid]["nrawtotal"] = int(float(line[5].strip()))
        dictAnaSample[dsid]["comment"] = line[6].strip()
        
    return dictAnaSample

def ReadCsvToDict_floats(csvfilename):
    csvdict = {}
    csvfile = open(csvfilename,'rb')
    reader = csv.reader(csvfile)
    for line in reader:
        if line[0].startswith("#"): continue
        csvdict[float(line[0])] = float(line[1])
    return csvdict