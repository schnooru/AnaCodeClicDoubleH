from ROOT import TFile,gDirectory,gROOT, TH1F, TH2F, TH2D
import sys
from HHTools import dictAnaSample_FromCSV
from clicpyana.ReadingTools import ReaderTools
from clicpyana.ReadingTools.ReadDatasetHistos import ReadDatasetHistos
from clicpyana.AnalysisTools import NormalizeHistos

import os.path

def DSID_from_hname(histname):
    DSID = histname.split("_")[ len(histname.split("_")  ) -1]
    return DSID

def RetrieveHistos_FromFilePath(Filename,SamplesDict):
    """ Based on ReadDatasetHistos, this reads a file and
    puts the histograms in a Histograms dictionary

    """

    filepath = Filename
    gDirectory.cd()
    AllHistos = {}
    if not os.path.exists(filepath) or not filepath.endswith(".root"):
        print("The file {} does not exist or is not a root file. Assuming you want to ignore it and trying to do nothing here.".format(filepath))
        return
        

    AllHistos = ReadDatasetHistos(AllHistos, Filename, SamplesDict).Histograms
    #at this point  within the RetrieveHistos_FromFilePath function the 2D histograms need to be converted in 1D histograms (basically just lining up the rows, and making a printout with the binning?? but then one should rebin already here... only the 2D histograms though, because for 1D it's more convenient to rebin in the FITPLOT step)

    #treat the 2D histograms in the correct way:
    #- apply the binvectors to x and y axis
    
    #- loop through y axis and make a new hist with putting them into a row
    AllHistos = Treat2Dhistos(AllHistos)


    return AllHistos

def Treat2Dhistos(allhistodict):


    allhistodict_new = {}
    for dsid in allhistodict:

        allhistodict_new[dsid] = []

        for histo in allhistodict[dsid]:


            if not type(histo) == TH2F and not type(histo) == TH2D:
                allhistodict_new[dsid].append( histo)
                continue
            newhist = Convert2Dto1D(histo)
            allhistodict_new[dsid].append(newhist)

    return allhistodict_new

def Convert2Dto1D(histo):

    hname = histo.GetName()
    htitle = histo.GetTitle()
    nbinsx = histo.GetXaxis().GetNbins()
    nbinsy = histo.GetYaxis().GetNbins()
    
    hnbins = nbinsx * nbinsy #completely ignoring any overflow and underflow
    histo_new = TH1F(hname+"dummy", htitle, hnbins, 0, hnbins ) #this is needed to remove the potential memory leak


    for ny in xrange(nbinsy + 1):
        #ignore underflow
        if ny == 0:
            continue
        ylow_bin = histo.GetYaxis().GetBinLowEdge(ny)
        yup_bin  = histo.GetYaxis().GetBinUpEdge(ny) 
        for nx in xrange(nbinsx + 1):
            #0...nbinsx
            #ignore underflow
            if nx == 0:
                continue
            xlow_bin = histo.GetXaxis().GetBinLowEdge(nx)
            xup_bin  = histo.GetXaxis().GetBinUpEdge(nx) 


            oldcontent = histo.GetBinContent(nx, ny)
            n_newhist = nx + nbinsx*( ny -1)
            #print("Is this the correct new bin number? nx={}, ny={}, n_newhist={}".format(nx,ny,n_newhist))
            histo_new.SetBinContent(n_newhist  ,oldcontent )
            histo_new.GetXaxis().SetBinLabel( n_newhist , "{}-{}&{}-{}".format(xlow_bin, xup_bin, ylow_bin, yup_bin) )
            histo_new.GetXaxis().SetTitle(hname)
            histo_new.GetYaxis().SetTitle("events")

    histo.Delete()
    histo_new.SetName( hname  )
    return histo_new
    
def readHistosInNormHisto(runConfig ,DontNormalize=False):
    """Reads in the Histogram files specified in runConfig (retrieves with RetrieveHistos_FromFilePath)
    and puts them in a NormHisto object, and also normalizes them
    """

    confPath = runConfig["ConfigFolder"]
    samplesDict_Sig = dictAnaSample_FromCSV(confPath + runConfig["Run_Sig_DsidCsvFile"])
    samplesDict_BG = dictAnaSample_FromCSV(confPath + runConfig["Run_BG_DsidCsvFile"])

    histoConfig =  ReaderTools.configDictFromJson(confPath + runConfig["Run_Histo_Config"])


    
    histos_nocuts_Sig = RetrieveHistos_FromFilePath(runConfig["Run_overallTag"]+"/"+runConfig["Proc_Sig_nocuts_histoFile"], samplesDict_Sig)
    histos_presel_Sig = RetrieveHistos_FromFilePath(runConfig["Run_overallTag"]+"/"+runConfig["Proc_Sig_presel_histoFile"], samplesDict_Sig)
    histos_BDT_Sig = RetrieveHistos_FromFilePath(runConfig["Run_overallTag"]+"/"+runConfig["Proc_Sig_BDT_histoFile"], samplesDict_Sig)

#
    histos_nocuts_BG = RetrieveHistos_FromFilePath(runConfig["Run_overallTag"]+"/"+runConfig["Proc_BG_nocuts_histoFile"], samplesDict_BG)
    histos_presel_BG = RetrieveHistos_FromFilePath(runConfig["Run_overallTag"]+"/"+runConfig["Proc_BG_presel_histoFile"], samplesDict_BG)
    histos_BDT_BG = RetrieveHistos_FromFilePath(runConfig["Run_overallTag"]+"/"+runConfig["Proc_BG_BDT_histoFile"], samplesDict_BG)
#

    ProcessGroupDict_BG = ReaderTools.configDictFromJson(confPath + runConfig["Proc_BG_Group_Config"])
    ProcessGroupDict_Sig = ReaderTools.configDictFromJson(confPath + runConfig["Proc_Sig_Group_Config"])

    
    #print("After trying to retrieve them, this is the histo dict:\n {}".format(histos_BDT_BG))
    ### initialize a NormalizeHistos object with everything it needs
    NormHisto = NormalizeHistos.NormalizeHistos( samplesDict_Sig, samplesDict_BG,  histos_nocuts_Sig, histos_presel_Sig, histos_BDT_Sig, histos_nocuts_BG, histos_presel_BG, histos_BDT_BG, ProcessGroupDict_BG,ProcessGroupDict_Sig , runConfig["Lumi"], runConfig["Polarization_factor_SIG"] )


    
    #NormHisto.NormalizeBG("fromNsample")
    if not DontNormalize:
        NormHisto.NormalizeBG("AnaVarMethod")
        
    #print("Normalized BG yields for each DSID")
    #print NormHisto.PrintHistosIntegrals( NormHisto.histos_BDT_BG)
    


    ###use the ReadHHanaChain class to get the Start event numbers from the anaChain files
    ###put them in a dictionary for each signal DSID
    ### pass this to some NormalizeSig function in the NormalizeHistos class

           
    if not DontNormalize:
        print("\nNormalizing the Signal samples")
        NormHisto.NormalizeSig()

    return NormHisto


def Debugprintout(NormHisto):

#    #####print NormHisto.PrintHistosIntegrals( NormHisto.histos_BDT_Sig)
#
    print("Number of entries only in 7181 (1.0,1.0):")
    print NormHisto.GrepDSID(NormHisto.PrintHistosEntries( NormHisto.histos_BDT_Sig),"7181")
    print("Number of events (weighted) only in 7181 (1.0,1.0):")
    print NormHisto.GrepDSID(NormHisto.PrintHistosIntegrals( NormHisto.histos_BDT_Sig),"7181")
    print("Number of events (weighted) only in 8197 (not grouped):")
    print NormHisto.GrepDSID(NormHisto.PrintHistosIntegrals( NormHisto.histos_BDT_Sig),"8197")

    print("Number of entries only in 8197:")
    print NormHisto.GrepDSID(NormHisto.PrintHistosEntries( NormHisto.histos_BDT_Sig),"8197")


    print("\nNumber of events (weighted) only in 8185 (gHHH=0.8)  (not grouped):")
    print NormHisto.GrepDSID(NormHisto.PrintHistosIntegrals( NormHisto.histos_BDT_Sig),"8185")
    print("Number of events (weighted) only in 8201 (gHHH=1.2)  (not grouped):")
    print NormHisto.GrepDSID(NormHisto.PrintHistosIntegrals( NormHisto.histos_BDT_Sig),"8201")

    
    return

    
def main(*args):
    """
    Reads the Histogram files produced by runDoubleH.py

    
    """

    if len(args) > 1:
        runConfigFile = args[1]
    else:
        sys.exit("Please provide a run configuration file as argument, e.g. ../Configs/runConfig_2016.json")
        
    print("Using this config file: {}".format(runConfigFile))
    
    runConfig = ReaderTools.configDictFromJson(runConfigFile)
    confPath = runConfig["ConfigFolder"]
    groupConfig_BG = ReaderTools.configDictFromJson(confPath + runConfig["Proc_BG_Group_Config"])
    groupConfig_Sig = ReaderTools.configDictFromJson(confPath + runConfig["Proc_Sig_Group_Config"])
    NormHisto = readHistosInNormHisto(runConfig )


    Debugprintout(NormHisto)


    print("The grouped Background event yields for lumi {}/fb".format(NormHisto.lumi))
    print NormHisto.GroupResultsTable(NormHisto.histos_BDT_BG, groupConfig_BG)
    
    #next step here should be to write the histograms to *weighted* histogram files

    BGoutputfile = runConfig["Run_overallTag"]+"/"+runConfig["Proc_BG_BDT_histoFile_wgtout"]
    BGoutf = TFile.Open(BGoutputfile,"RECREATE")
    
    groupedHistos_BG = NormHisto.GroupHistograms(NormHisto.histos_BDT_BG, groupConfig_BG)
    
    print NormHisto.PrintHistosIntegrals(groupedHistos_BG)

    print("Precision (HHbbbb) = {:.3f}".format(NormHisto.CalculatePrecision(groupedHistos_BG)))
    Sig = float(NormHisto.GrepDSID(NormHisto.PrintHistosIntegrals( NormHisto.histos_BDT_Sig),"7181").split("  ")[1])

    print("Precision (HH->all)= {:.3f}".format(NormHisto.CalculatePrecision_Sig(Sig, groupedHistos_BG)))
    print("THESE MULTIPLIED WITH 1.47 TO GET THE LAMBDA PRECISION:")
    print("\Delta \lambda/\lambda (HHbbbb) = {:.3f}".format(1.47*float(NormHisto.CalculatePrecision(groupedHistos_BG))))
    print("\Delta \lambda/\lambda (HH->all)= {:.3f}".format(1.47*float(NormHisto.CalculatePrecision_Sig(Sig, groupedHistos_BG))))

    for proc in groupedHistos_BG.keys():
        for h in groupedHistos_BG[proc]:
            h.Write()
    #    print NormHisto.histos_BDT_BG[dsid]
    print("Writing background histograms to {}".format(BGoutf.GetName()))

    BGoutf.Close()


    #for signal, they also have to be grouped in case of statistics extension by different DSID
    Sigoutputfile = runConfig["Run_overallTag"]+"/"+runConfig["Proc_Sig_BDT_histoFile_wgtout"]
    Sigoutf = TFile.Open(Sigoutputfile,"RECREATE")
    groupedHistos_Sig = NormHisto.GroupHistograms(NormHisto.histos_BDT_Sig, groupConfig_Sig  )
    for DSID in groupedHistos_Sig.keys():
        for h in groupedHistos_Sig[DSID]:
            h.Write()
    print("Writing signal histograms to {}".format(Sigoutf.GetName()))
    Sigoutf.Close()

    
    
    return
    
if __name__ == "__main__":
    main(*sys.argv)


    
