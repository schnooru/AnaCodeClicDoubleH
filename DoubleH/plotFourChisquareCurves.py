from ROOT import TGraph, TCanvas, TF1, kAzure, kGreen, kMagenta, kRed, kOrange, gStyle, gPad, TLegend, TText, kBlack, kWhite, TLatex





def polynomial(x,par):
    parabola = par[0]*x[0]**4 + par[1]*x[0]**3 + par[2]*x[0]**2 + par[3]*x[0] + par[4]
    return  parabola

def main():
    gStyle.SetLineWidth(2)
    gStyle.SetLabelSize(0.07)

    gStyle.SetNumberContours(50)
    gStyle.SetPadTickY(1)
    gStyle.SetPadTickX(1)

    
        
    c = TCanvas()
    c.SetBottomMargin(0.2)
    c.SetLeftMargin(0.2)
    dummy = TF1( "",polynomial , 0.45, 2.55, 5 )
    dummy.SetLineColor(kWhite)

    leg = TLegend(0.27,0.615,0.7,0.9)
    leg.SetBorderSize(0)
    leg.SetFillStyle(0)
    #formula_1bin_withoutZhh = polynomial(x)
    f_1bin_withoutZhh = TF1( "",polynomial , 0.45, 2.55, 5 )
    f_1bin_withoutZhh.SetParameters(3.94274e+01,-2.74538e+02,6.69448e+02,-6.66480e+02,2.32571e+02-0.30530805296)
    f_1bin_withoutZhh.SetLineColor(kGreen-3)
    f_1bin_withoutZhh.SetLineStyle(5)
    f_1bin_withoutZhh.GetYaxis().SetLabelSize(0.07)
    f_1bin_withoutZhh.GetYaxis().SetTitle("    #Delta #chi^{2}")
    f_1bin_withoutZhh.GetYaxis().SetTitleSize(0.07)

    f_1bin_withoutZhh.GetXaxis().SetTitle("g_{HHH}/g^{SM}_{HHH}   ")
    f_1bin_withoutZhh.GetXaxis().SetTitleSize(0.07)

    
    f_1bin_withoutZhh.SetLineWidth(3)
    f_1bin_withoutZhh.Draw()

    f_1bin_withZhh = TF1( "formula_1bin_withZhh",polynomial , 0.45, 2.55, 5 )
    f_1bin_withZhh.SetParameters( 3.76780e+01,-2.64997e+02, 6.58519e+02,-6.66699e+02, 2.35929e+02-0.331828844938    )
    f_1bin_withZhh.SetLineStyle(7)
    f_1bin_withZhh.SetLineColor(kGreen+3)
    f_1bin_withZhh.SetLineWidth(3)
    f_1bin_withZhh.Draw("same")



    f_15bins_withZhh = TF1( "formula_15bins_withZhh",polynomial , 0.45, 2.55, 5 )
    f_15bins_withZhh.SetParameters(7.84360e+01,-4.96895e+02, 1.15747e+03, -1.13970e+03,4.02309e+02-1.61995362672)
    f_15bins_withZhh.SetLineWidth(4)

    f_15bins_withZhh.SetLineColor(kAzure-6)

    f_15bins_withZhh.Draw("same")

    f_15bins_withoutZhh = TF1( "formula_15bins_withoutZhh",polynomial , 0.45, 2.55, 5 )
    f_15bins_withoutZhh.SetParameters( 7.95457e+01,-5.02550e+02, 1.16025e+03,-1.13244e+03,3.96819e+02-1.61850772491)
    f_15bins_withoutZhh.SetLineStyle(7)
    f_15bins_withoutZhh.SetLineWidth(3)
    f_15bins_withoutZhh.SetLineColor(kAzure+6)
    f_15bins_withoutZhh.Draw("same")




    leg.AddEntry(f_1bin_withoutZhh, "rate only, HH#nu#nu","l")
    leg.AddEntry(f_1bin_withZhh, "rate only, HH#nu#nu & ZHH","l")
    leg.AddEntry(f_15bins_withoutZhh, "differential, HH#nu#nu","l")
    leg.AddEntry(f_15bins_withZhh, "differential, HH#nu#nu & ZHH","l")
    leg.SetMargin(0.12)
    leg.Draw()
    clictext = "CLICdp "
       
    t1 = TText(0.5,0.5,clictext)
    t1.SetTextSize(0.05)
    t1.SetTextColor(kBlack)
    t1.SetTextFont(42)
    t1.DrawTextNDC(0.2,0.91, clictext)

   

    # t1 = TText(0.5,0.5,clictext)
    # t1.SetTextSize(0.05)
    # t1.SetTextColor(kBlack)
    # t1.SetTextFont(42)
    # t1.DrawTextNDC(0.4,0.8, clictext)

    opttext1 = "HH#nu#nu: 3 TeV; 5 ab^{-1}"
    opttext2 = "ZHH: 1.4 TeV; 2.5 ab^{-1}"
    #t1.DrawLatex(0.6,0.9,opttext1)

    smallt = TLatex()
    smallt.SetTextSize(0.05)
    smallt.SetTextFont(42)
    smallt.SetTextColor(kBlack)
    smallt.DrawLatex(0.6,0.9,opttext1+"; "+opttext2)

    
    c.Modified()
    c.Update()
    raw_input()
    c.Close()
    return

if __name__ == "__main__":
    main()
    
# Double_t myFunc(double x) { return x+sin(x); }
# ....
# TF1 *fa3 = new TF1("fa3","myFunc(x)",-3,5);
# fa3->Draw();


# polynomial =  par[0]*x[0]**4 + par[1]*x[0]**3 + par[2]*x[0]**2 + par[3]*x[0] + par[4]
