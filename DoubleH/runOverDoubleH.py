from ROOT import TFile
from clicpyana.ReadingTools import ReaderTools

import sys, os
from HHTools import dictAnaSample_FromCSV
from clicpyana.AnalysisTools.HistogramCreator import HistogramCreator
from clicpyana.ReadingTools.ReadHHAnaChain import ReadHHAnaChain
from clicpyana.ReadingTools.ReadHHtmvafile import ReadHHtmvafile

def FillHistos_Sig(AllHistos, samplesDict_Sig,histoConfig, cutsDict):
    print("Running over signals...........................")
    for dsid in samplesDict_Sig.keys():
        Histos_dsid = HistogramCreator(histoConfig, dsid).histsdict
        pathToSignalFile = samplesDict_Sig[dsid]["path"]
        eventReader = ReadHHAnaChain(Histos_dsid, dsid, cutsDict ,pathToSignalFile )
        AllHistos[dsid] = Histos_dsid
        
    return AllHistos

def FillHistos_BG(AllHistos, samplesDict_BG,histoConfig,BG_tmva_file,cutsDict,TreeName):
    print("Running over BG...........................")
    for dsid in samplesDict_BG.keys():
        Histos_dsid = HistogramCreator(histoConfig, dsid).histsdict
        AllHistos[dsid] = Histos_dsid
    #print AllHistos
        
        ### two cases: presel file has BDT3, nocutsfile does not
    #print(BG_tmva_file.split("_"))
    Has_BDT3 = "presel" in BG_tmva_file.split("_") or "13may2018.root" in BG_tmva_file.split("_") 

    print("Running for the case that BDT3 variable can be filled: {}".format(Has_BDT3))
    eventReader = ReadHHtmvafile(AllHistos,BG_tmva_file, cutsDict,TreeName,Has_BDT3)
    

        
    return AllHistos

    
def WriteHistos(AllHistos, histoFile):

    histoFile.cd()
    for dsid in AllHistos.keys():
        #dsids
        for h in AllHistos[dsid]:
            AllHistos[dsid][h].Write()
    print("Saving the histograms in {}".format(histoFile.GetPath()).strip(":/"))
    histoFile.Close()
     
    return
    
def runOverDoubleH(runConfig) :
    """
    runs over an HH tmva file and creates a histo file

    """

    ### Get more things from the configuration
    confPath = runConfig["ConfigFolder"]
    selectionConfig = ReaderTools.configDictFromJson(confPath +runConfig["Run_Cuts_Config"])
    histoConfig =  ReaderTools.configDictFromJson(confPath + runConfig["Run_Histo_Config"])

    cutsDict = {}
    cutsDict["BDT_min"]= selectionConfig["BDT_min"]
    cutsDict["y34_min"]= selectionConfig["y34_min"]
    cutsDict["sumbtag_min"]= selectionConfig["sumbtag_min"]

    print("Using BDT > {}".format(cutsDict["BDT_min"]))
    samplesDict_Sig = dictAnaSample_FromCSV(confPath + runConfig["Run_Sig_DsidCsvFile"])
    samplesDict_BG = dictAnaSample_FromCSV(confPath + runConfig["Run_BG_DsidCsvFile"])
    BG_tmva_file =  confPath + runConfig["Run_BG_tmvafile"]


    
    ### Prepare Histogram dictionaries
    AllHistos_Sig = {}
    AllHistos_BG = {}
    ### Prepare output files
    if not os.path.exists(runConfig["Run_overallTag"]):
        os.makedirs(runConfig["Run_overallTag"])
    filepath_Sig = runConfig["Run_overallTag"] + "/"+runConfig["Run_Sig_outputFile"]
    histoFile_Sig = TFile.Open(filepath_Sig,"RECREATE")
    filepath_BG = runConfig["Run_overallTag"] + "/"+runConfig["Run_BG_outputFile"]
    histoFile_BG = TFile.Open(filepath_BG,"RECREATE")


    if "Sig" in runConfig["Run_SigBG"].split("_"):
        AllHistos_Sig = FillHistos_Sig(AllHistos_Sig , samplesDict_Sig,histoConfig, cutsDict )
        WriteHistos(AllHistos_Sig, histoFile_Sig)

    if "BG" in runConfig["Run_SigBG"].split("_"):
        AllHistos_BG = FillHistos_BG(AllHistos_BG , samplesDict_BG, histoConfig,BG_tmva_file, cutsDict , runConfig["Run_TreeName"])
        WriteHistos(AllHistos_BG, histoFile_BG)

    
def main(*args):
    """
    Reads DoubleH signal and BG ntuples and fills Histograms for each DSID

    needs: input root files with trees, run config, sample info csv    
    """

    if len(args) > 1:
        runConfigFile = args[1]
    else:
        sys.exit("Please provide a run configuration file as argument, e.g. ../Configs/runConfig_2016.json")

    print("Using this config file: {}".format(runConfigFile))
    runConfig = ReaderTools.configDictFromJson(runConfigFile)




    runOverDoubleH( runConfig)
    return

    
if __name__ == "__main__":
    main(*sys.argv)

