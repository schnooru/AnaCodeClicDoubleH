from ROOT import TFile,gDirectory,gROOT, TH1F, gStyle, TH2F,TLegend, gPad, THStack, kRed, TText,kBlack, TMathText,TF1,TGraph,kGreen, TRandom3, TGraph2D,gEnv, TPolyMarker, TH2D, kMagenta, kPink, kOrange, kViolet,kCyan, gPad, TList, TMath, kBlue, TGraphErrors, kAzure
from clicpyana.ReadingTools.ReadDatasetHistos import ReadDatasetHistos
from clicpyana.ReadingTools import ReaderTools
import sys
from math import sqrt, exp
from CouplingsVary import CouplingsVary
from HHTools import dictAnaSample_FromCSV, ReadCsvToDict_floats
from clicpyana.PlottingTools import Plotter
from clicpyana.AnalysisTools.TemplateFit import TemplateFit
from TemplateObjects import TemplateObjects
from PlotHistos import PlotHistos
from HistoPrepare import HistoPrepare
import sys
sys.path.append('/usr/lib64/python2.6/site-packages')
import numpy as np

from array import array



def SaveCanvas( canv, histobject, name ):
    save_name = name

    
    canv.Modified()
    canv.Update()
    save_it = raw_input("Save this plot as {}? [y/N]".format(save_name)  )=="y"
    if save_it:
        canv.SaveAs("{}".format(name))
        canv.SaveAs("{}".format(name.replace(".pdf",".C")))


    if type(histobject) == THStack:
        for i in histobject.GetHists():

            i.Delete()
    del histobject

        
    canv.Close()
    return

def Chi2_from_gHHH_gHHWW( gHHHvalue, gHHWWvalue, TemplateInfo, Chi2dict):
    #get DSID from gHHH value (where gHHWW=1.0)

    dsid = TemplateInfo.DSID_from_gHHH_gHHWW(  gHHHvalue, gHHWWvalue)
    return Chi2dict[dsid]

def Error_from_gHHH_gHHWW ( gHHHvalue, gHHWWvalue, TemplateInfo, abserrordict ,Chi2dict  ):
    dsid = TemplateInfo.DSID_from_gHHH_gHHWW(  gHHHvalue, gHHWWvalue)
    return abserrordict[dsid] # it's already the absolute error

def Plot2DHistos( HistoPrep, Plotting, runConfig ):
    # ====== Plot the 2D couplings behaviour plots ==========================================================
    histoConfig_xsec2D = { "Title": "total cross section [fb]",
                           "xTitle": "g_{HHH}/g^{SM}_{HHH}", "yTitle": "g_{HHWW}/g_{HHWW}^{SM}", "lumi":  0,
                           "polarization" : runConfig["Polarization_factor_SIG"] }

    
    #Plotting = PlotHistos(runConfig)
    canv, xsecplot = Plotting.Plot2D(  HistoPrep.CrossSectionHistogram2D(),histoConfig_xsec2D,False)
    SaveCanvas(canv, xsecplot, "xsec_histo.pdf")

   

    histoConfig_eventyields = { "Title": "Event yields in signal region",
                                "xTitle": "g_{HHH}/g^{SM}_{HHH}", "yTitle": "g_{HHWW}/g_{HHWW}^{SM}", "lumi":  runConfig["Lumi"], "polarization" : runConfig["Polarization_factor_SIG"]}
    canv, evyieldplot = Plotting.Plot2D(  HistoPrep.EventYieldHisto(),histoConfig_eventyields,False)
    SaveCanvas(canv,evyieldplot,"eventyields.pdf")
    return

def Plot1DgHHHxsec_HHnunu3TeV( runConfig, HistoPrep, Plotting, DsidList1D):
    histoConfig = { "Title": "HH#nu#nu 3TeV",
                    "xTitle": "g_{HHH}/g^{SM}_{HHH}", "yTitle": "#sigma [fb]",
                    "lumi":  runConfig["Lumi"], "polarization" : runConfig["Polarization_factor_SIG"]}
    xsecgraph = HistoPrep.CrossSectionGraph1D(DsidList1D)
    leg = TLegend(0.35,0.75,0.7,0.82)
    canv, plot = Plotting.Plot1D(xsecgraph, histoConfig, WIP=True)
    plot.GetYaxis().SetTitleOffset(0.9)
    leg.AddEntry(plot,histoConfig["Title"],"p")
    leg.Draw()
    SaveCanvas(canv,plot,"xsec_vs_gHHH_1D.pdf")
    return

def Plot1DgHHHxsec_Zhh1400GeV(confPath, runConfig, Plotting ):
    histoConfig = { "Title": "ZHH 1.4 TeV",
                    "xTitle": "g_{HHH}/g^{SM}_{HHH}", "yTitle": "#sigma [fb]",
                    "lumi":  runConfig["Lumi"],"polarization" : runConfig["Polarization_factor_SIG"]}
    ZHHxsecfile = confPath+runConfig["Fit_Zhh_file"]
    xsecdict = ReadCsvToDict_floats(ZHHxsecfile)
    gHHHlist = xsecdict.keys()
    gHHHlist.sort()
    gHHHarray = np.array(gHHHlist)
    xseclist = [xsecdict[g] for g in gHHHlist]
    xsecarray = np.array(xseclist)

    xsecgraph = TGraph(len(gHHHarray), gHHHarray, xsecarray)
    leg = TLegend(0.35,0.75,0.7,0.82)
    canv, plot = Plotting.Plot1D(xsecgraph, histoConfig, WIP=True)
    plot.GetYaxis().SetTitleOffset(1.)
    leg.AddEntry(plot,histoConfig["Title"],"p")
    leg.Draw()
    SaveCanvas(canv,plot,"xsec_vs_gHHH_1D_Zhh.pdf")

    return

def Plot1DSMdistributions (  histoConfigDict, runConfig, confPath, HistoPrep, Plotting ,histnames=None):
    if not histnames:
        histnames =[runConfig["Fit_Observable"]]
    for histname in histnames:
        print("Working on the 1D SM distributions for {}".format(histname))
        configSMHistos = histoConfigDict[histname]
        configSMHistos["histname"]          = histname
        configSMHistos["ordered_processes"] = runConfig["Fit_ordered_allSM_processes"]
        configSMHistos["range"]             = histoConfigDict[histname]["post_range"]
        configSMHistos["procdict"]          = ReaderTools.configDictFromJson( confPath + runConfig["Plot_Legend_BG"])
        configSMHistos["Title"]             = ""
        configSMHistos["xTitle"]            =  histoConfigDict[histname]["xtitle"]
        configSMHistos["yTitle"]            =  histoConfigDict[histname]["ytitle"]
        configSMHistos["lumi"]              = runConfig["Lumi"]
        configSMHistos["polarization"]      = runConfig["Polarization_factor_SIG"]
        if not configSMHistos["type"] == "TH2D" and not configSMHistos["type"] == "TH2F" and not configSMHistos["useBinvec"] == 0:
            configSMHistos["useBinvec"] = 1

            #print histname
            #print  configSMHistos["type"]
            binarray = array('d')
            [binarray.append(x) for x in configSMHistos["binvec"]]
            configSMHistos["BinArray"] = binarray

        else:
            configSMHistos["useBinvec"] = 0

        histo, legend = HistoPrep.Stack_SM_list(  configSMHistos )
        #print    HistoPrep.TemplateObjects.histos_Sig["7163"][5].GetXaxis().GetBinLabel(1)

        if histname == runConfig["Fit_Observable"]:
            processList = runConfig["Fit_ordered_BG_processes"]
            histo_BG_fit = HistoPrep.GetTotalHistFromStackProcessList( histo ,configSMHistos, processList )
            HistoPrep.SetBGHistoForFit( histo_BG_fit)
        canv, smplot, legend , pads = Plotting.PlotFromOneStack( histo  ,legend , configSMHistos ,"vs" in histname )

        factor_xrange = 1.2
        smplot.SetMaximum( factor_xrange*smplot.GetMaximum())

        canv.Modified()
        canv.Update()

        if "vs" in histname:
            pads[0].cd()
            smplot,axis, canv,xaxis = Plotting.TreatKinematic2Dplots(canv,smplot,factor_xrange*smplot.GetMaximum(),pads)
        canv.Modified()
        canv.Update()

        SaveCanvas(canv,  smplot, "{}.pdf".format(histname))

    return


def  PlotBGPlusListOfSignals1D( histoConfigDict, runConfig, confPath, HistoPrep, Plotting, TemplateInfo ,histnames   ):
    """
    Plot BG plus a list of signals, 1D distributions
    """
    SigDsidList = runConfig["Plot_Signals"]
    print("Working on the 1D distributions with BG and the following signals: {}".format( ", ".join( map(str,SigDsidList)  )  ))

    for histname in histnames:
        print("For the {} distribution".format(histname))
        signals_dict ={}
        signals_dict["signalhist"]=[]
        signals_dict["signalstack"]=[]
        for dsid in SigDsidList:
            configHistos                      = histoConfigDict[histname]
            configHistos["histname"]          = histname
            configHistos["ordered_processes"] = runConfig["Fit_ordered_BG_processes"]#important!
            configHistos["range"]             = histoConfigDict[histname]["post_range"]
            configHistos["procdict"]          = ReaderTools.configDictFromJson( confPath + runConfig["Plot_Legend_BG"])
            configHistos["Title"]             = ""
            configHistos["xTitle"]            =  histoConfigDict[histname]["xtitle"]
            configHistos["yTitle"]            =  histoConfigDict[histname]["ytitle"]
            configHistos["lumi"]              = runConfig["Lumi"]
            configHistos["polarization"]      = runConfig["Polarization_factor_SIG"]
        
            if not configHistos["type"] == "TH2D" and not configHistos["type"] == "TH2F"  and not configHistos["useBinvec"] == 0:
                configHistos["useBinvec"] =1
                binarray = array('d')
                [binarray.append(x) for x in configHistos["binvec"]]
                configHistos["BinArray"] = binarray
            else:
                configHistos["useBinvec"] = 0

            stack_BG, legend = HistoPrep.Stack_SM_list(  configHistos   )
            histo_signal =  HistoPrep.Retrieve1DHisto_Signal( histname , dsid, configHistos  )

            signals_dict["signalhist"].append(histo_signal)


        #signals_colors = [  kOrange+8,kMagenta+2,kGreen+3, kPink-8 ]
        signals_colors = [  kOrange+8,kMagenta+2,kGreen+3, kPink-8 ]
        n=0
        
        for h in signals_dict["signalhist"]:
            n+=1
            h.SetLineColor(kMagenta-2*n)
            if n<=len(signals_colors):
                h.SetLineColor(signals_colors[n-1])
            h.SetLineWidth(3)
            h.SetLineStyle(n)
            stack_with_sig =   stack_BG.Clone()
            stack_with_sig.Add(h)
            signals_dict["signalstack"].append(stack_with_sig)
            dsid = h.GetName().split("_")[1]
            gHHH      = float( TemplateInfo.gHHH_from_DSID (dsid)  )
            gHHWW     = float( TemplateInfo.gHHWW_from_DSID(dsid)  )
            if gHHH == 1.0 and gHHWW == 1.0:
                legend.AddEntry( h,"ee #rightarrow HH #nu #bar{#nu}" , "l"  )
            else:
                legend.AddEntry( h,"{} = {}; {} = {}".format("g_{HHH}/g^{SM}_{HHH}",gHHH,  "g_{HHWW}/g^{SM}_{HHWW}" ,gHHWW) , "l"  )


        ymax=1000
        canv, smplot, legend, pads = Plotting.PlotFromStacks(  signals_dict["signalstack"]  ,legend , configHistos,"vs" in histname, ymax=ymax )
        gPad.RedrawAxis()

        if "vs" in histname:
            pads[0].cd()
            smplot,axis, canv,xaxis = Plotting.TreatKinematic2Dplots(canv,smplot[0],ymax,pads)

        
        canv.Modified()
        canv.Update()

        SaveCanvas(canv, smplot,  "{}_with_{}.pdf".format(histname,"_".join(map(str,SigDsidList))))
    return



def PlotSignalsOnly1D( histoConfigDict, runConfig, confPath, HistoPrep, Plotting, TemplateInfo ,histnames   ):
    """
    Plot various signals, 1D distributions (signals given in runConfig["Plot_Signals"] )
    """
    SigDsidList = runConfig["Plot_Signals"]
    print("Working on the 1D distributions of the following signals: {}".format( ", ".join( map(str,SigDsidList)  )  ))

    for histname in histnames:
        print("For the {} distribution".format(histname))
        signals_dict ={}
        signals_dict["signalhist"]=[]
        signals_dict["signalstack"]=[]
        for dsid in SigDsidList:
            configHistos = histoConfigDict[histname]
            configHistos["histname"] = histname
            configHistos["ordered_processes"] = runConfig["Fit_ordered_BG_processes"]#important!
            configHistos["range"] = histoConfigDict[histname]["post_range"]
            configHistos["procdict"] = ReaderTools.configDictFromJson( confPath + runConfig["Plot_Legend_BG"])
            configHistos["Title"] = ""
            configHistos["xTitle"] =  histoConfigDict[histname]["xtitle"]
            configHistos["yTitle"] =  histoConfigDict[histname]["ytitle"]
            configHistos["lumi"] = runConfig["Lumi"]
            configHistos["polarization"]    = runConfig["Polarization_factor_SIG"]

            
            if not configHistos["type"] == "TH2D" and not configHistos["type"] == "TH2F"  and not configHistos["useBinvec"] == 0:
                configHistos["useBinvec"] =1
            
                binarray = array('d')
                [binarray.append(x) for x in configHistos["binvec"]]
                configHistos["BinArray"] = binarray
            else:
                configHistos["useBinvec"] = 0


            histo_signal =  HistoPrep.Retrieve1DHisto_Signal( histname , dsid, configHistos  )
            signals_dict["signalhist"].append(histo_signal)

        #signals_colors = [  kOrange+8,kMagenta+2,kGreen+3, kPink-8 ]
        #signals_colors = [  kOrange+8,kRed+2, kPink+6 , kGreen+3,kCyan, kCyan-2 ,kBlue+2 ]
        #signals_linestyle = [2,3,7,1,5,4,6]
        signals_colors = [   kGreen+3,kOrange+8,9, kCyan, kCyan-2 ,kBlue+2 ]
        signals_linestyle = [2,3,5,4,6]
        signals_colors = [ kCyan  ,kGreen+3,9,kOrange+7,kViolet+2, kCyan, kBlue+2 ]
        signals_linestyle = [4,2,5,6,3,6]
        ratio = False
        if ratio:
            canv = Plotter.createCanvas("canv",500,300)
            legend = TLegend(0.4,0.4,0.86,0.86  )
            legend.SetBorderSize(0)
            legend.SetFillStyle(0)
        else:
            canv = Plotter.createCanvas()
            legend = HistoPrep.CreateLegend()
        n=0
        print signals_dict["signalhist"]
        histoSM = HistoPrep.Retrieve1DHisto_Signal( histname , "7181", configHistos  )
        print histoSM
        for h in signals_dict["signalhist"]:
            n+=1
            h.SetLineColor(kMagenta-2*n)
            if n<=len(signals_colors):
                h.SetLineColor(signals_colors[n-1])
            h.SetLineWidth(4)
            h.SetLineStyle(n)
            if n<= len(signals_linestyle):
                h.SetLineStyle(signals_linestyle[n-1])
            #h.SetLineStyle(1)
            dsid = h.GetName().split("_")[1]
            gHHH      = float( TemplateInfo.gHHH_from_DSID (dsid)  )
            gHHWW     = float( TemplateInfo.gHHWW_from_DSID(dsid)  )
            #legend.AddEntry( h,"{} = {}; {} = {}".format("g_{HHH}/g^{SM}_{HHH}",gHHH,  "g_{HHWW}/g^{SM}_{HHWW}" ,gHHWW) , "l"  )
            legend.AddEntry( h,"{} = {}; {} = {}".format("#kappa_{HHH}",gHHH,  "#kappa_{HHWW}" ,gHHWW) , "l"  )
            if ratio:
                h.Divide(histoSM)

            if n==1:

                h.Draw("HIST")

                canv, h, t1,lumit = Plotting.SetGeneralStyle(canv, h, configHistos)
                h.GetXaxis().SetTitleSize(0.7*h.GetXaxis().GetTitleSize())
                h.GetYaxis().SetTitleSize(0.7*h.GetYaxis().GetTitleSize())
                h.GetYaxis().SetLabelSize(0.7*h.GetYaxis().GetTitleSize())
                h.GetXaxis().SetLabelSize(0.7*h.GetXaxis().GetTitleSize())
                h.GetXaxis().SetTitleOffset(0.8*h.GetXaxis().GetTitleOffset())
                h.GetYaxis().SetTitleOffset(0.6*h.GetYaxis().GetTitleOffset())
                if ratio:
                    h.GetYaxis().SetRangeUser(0,5)
                    h.GetYaxis().SetTitle("ratio to SM")
                    h.GetYaxis().SetLabelSize(0.06)
                    h.GetXaxis().SetLabelSize(0.06)

                else:
                    h.SetMaximum( 3*h.GetMaximum())
            else:
                h.Draw("HIST SAME")


                
        legend.Draw()
        canv.Modified()
        canv.Update()
        if ratio:
            h.GetYaxis().SetTitle("ratio to SM")
            h.GetYaxis().SetLabelSize(0.06)
            h.GetXaxis().SetLabelSize(0.06)
            SaveCanvas(canv, h,  "{}_SignalsRatios_{}.pdf".format(histname,"_".join(map(str,SigDsidList))))
        else:
            SaveCanvas(canv, h,  "{}_SignalsOnly_{}.pdf".format(histname,"_".join(map(str,SigDsidList))))
                        
    return

def FillChi2Graph( TemplateInfo, confPath, runConfig, histoConfigDict, HistoPrep , DsidList1D ,randgen , zhh_dict,HHvvStg2_dict, ThrowToys=False):
    """ Fill the Chi2 for the nominal case!
    this needs to be changed when doing this several times for the different variations, maybe (maybe not)
    """
    # ====== prepare for 1D fit
    # ====== retrieve all the gHHWW = 1 histograms and add them to a dictionary


    DictHistSignals            = {}
    # it needs to be the signal histograms after running the Prepare step (rebinning etc)
    configHistosProcs          = {}
    FitObservable              = runConfig["Fit_Observable"]
    configHistosProcs          = histoConfigDict[FitObservable]
    configHistosProcs["rebin"] = histoConfigDict[FitObservable]["rebin"]
    configHistosProcs["range"] = histoConfigDict[FitObservable]["post_range"]
    if not configHistosProcs["type"] == "TH2D" and not  configHistosProcs["type"] == "TH2F"  and not configHistosProcs["useBinvec"] == 0:
        configHistosProcs["useBinvec"] = 1

        binarray = array('d')
        [binarray.append(x) for x in configHistosProcs["binvec"]]
        configHistosProcs["BinArray"] = binarray
    else:
        configHistosProcs["useBinvec"] = 0

    for dsid in DsidList1D:
        hist = HistoPrep.Retrieve1DHisto_Signal( FitObservable , dsid,configHistosProcs  )
        DictHistSignals [dsid] = hist

    #FitObs_SMSig_orig = HistoPrep.Retrieve1DHisto_Signal( FitObservable, 7181 ,configHistosProcs )
    FitObs_SMSig = HistoPrep.Retrieve1DHisto_Signal( FitObservable, 7181 ,configHistosProcs )

    # ====== Perform the template fit ========================================================================
    #use as BG only histogram: histo_BG_fit
    #use for the SM HH histogram:BDT_SMSig 
    #use for the couplings histograms: DictHistSignals

    #if ThrowToys == True, then the OBSERVED histogram must be varied bin by bin. How is the OBSERVED histogram handed to the routines performing the chi2 calculation? Mainly by initializing the TemplateFit object with it, i.e. FitObs_SMSig.

    #if ThrowToys:
    #    FitObs_SMSig = ToyVaryHist( FitObs_SMSig_orig , randgen)
    #else:
    #    FitObs_SMSig = FitObs_SMSig_orig
    verbose = False
    TmplFit                 = TemplateFit(HistoPrep.histo_BG_fit , FitObs_SMSig , DictHistSignals,TemplateInfo, randgen, ThrowToys )
    FitObservable           = runConfig["Fit_Observable"]
    addZhhtoChi2            = runConfig["Fit_Include_Zhh14"]


    if addZhhtoChi2: print ("Including the Zhh point at 1.4 TeV in the fit")

    
    addStg2HHvvtoChi2       = runConfig["Fit_Include_HHvv14"]
    if addStg2HHvvtoChi2: print ("Including the HHvv point at 1.4 TeV in the fit")

    Chi2dict, errordict  = TmplFit.GetChisquaresOfHistos(FitObservable,addZhhtoChi2, zhh_dict,addStg2HHvvtoChi2,HHvvStg2_dict)
    
    

    canv1                   = Plotter.createCanvas()

    gHHHlist   = []
    chi2list   = []

    for dsid in Chi2dict:
        gHHH       = float( TemplateInfo.gHHH_from_DSID (dsid)  )
        gHHWW      = float( TemplateInfo.gHHWW_from_DSID(dsid)  )
        if gHHWW == 1.0:
            gHHHlist.append( gHHH    )
            
            if verbose:
                print( "DSID={}, gHHH={}, gHHWW={}, chi2={:<2f}".format(dsid,gHHH, gHHWW, Chi2dict[dsid]))

    #need to first sort the gHHHlist, then make the array, and then fill according to the chi2 the chi2array...
    gHHHlist.sort()        
    gHHHarray = np.array(gHHHlist)

    #for YR plots#===
    ###dklarray = gHHHarray-1
    #===

    for gHHH in gHHHarray:        
        chi2list.append(Chi2_from_gHHH_gHHWW( gHHH, 1.0, TemplateInfo, Chi2dict)) #YR: subtract min.
    chi2array = np.array(chi2list)

    errorlist = []
    for gHHH in gHHHarray:
        errorlist.append(Error_from_gHHH_gHHWW ( gHHH,1.0, TemplateInfo, errordict ,Chi2dict  ))
    errorarray = np.array(errorlist)

    xerrorslist = []
    for i in xrange(len(errorlist)):
        xerrorslist.append(0)
    xerrors = np.array(xerrorslist)



    g=TGraphErrors(len(gHHHarray), gHHHarray, chi2array,xerrors,errorarray)
    #for YR plots#===
    ###g=TGraphErrors(len(gHHHarray), dklarray, chi2array,xerrors,errorarray)
    #===


    g.SetTitle("chi2")

    canv1.cd()

    g.Draw("AP")
    g.SetMarkerColor(9)
    g.SetMarkerStyle(21)#(21)
    g.SetMarkerSize(0.8)
    g.GetXaxis().SetTitle("g_{HHH}/g^{SM}_{HHH}")
    g.GetYaxis().SetTitle("#Delta #chi^{2}")
    if ThrowToys:
        g.GetYaxis().SetTitle("#chi^{2}")
    g.GetXaxis().SetRangeUser(0.5,2.7)
    g.GetYaxis().SetRangeUser(0,80)
    xtitle = "g_{HHH}/g^{SM}_{HHH}"
    #for YR plots:
    #g.GetXaxis().SetRangeUser(-0.5,1.7)
    #g.GetYaxis().SetRangeUser(0,20)
    #xtitle = "#Delta #kappa_{#lambda}"
    #g.GetYaxis().SetTitleSize(0.057)
    #g.GetXaxis().SetTitleSize(0.052)
    #===

    canv1.SetGridy()
    g.SetTitle("")
    canv1.Modified()
    canv1.Update()


    chi2histoconfig = {"Title": "", "xTitle" : xtitle, "yTitle": "#Delta #chi^{2}", "lumi" : runConfig["Lumi"] , "polarization" : runConfig["Polarization_factor_SIG"] }
    PlotChi2Histos = PlotHistos(runConfig, histoConfigDict)

    isWithZHH = runConfig["Fit_Include_Zhh14"]
    canv1, g, t1 ,lumit = PlotChi2Histos.SetGeneralStyleChi2(canv1,g,chi2histoconfig, isWithZHH, runConfig["Fit_extratext"])
    g.GetYaxis().SetTitleOffset(1.0)
    g.GetXaxis().SetTitleOffset(1.2)
    g.GetYaxis().SetRangeUser(0,30)

    return g, canv1


def FillChi2Graph2D(  TemplateInfo, runConfig, histoConfigDict, HistoPrep , DsidList,randgen, ThrowToys=False , IsUseGraph=True  ):


    DictHistSignals            = {}
    # it needs to be the signal histograms after running the Prepare step (rebinning etc)
    configHistosProcs          = {}
    FitObservable              = runConfig["Fit_Observable"]
    configHistosProcs          = histoConfigDict[FitObservable]
    configHistosProcs["rebin"] = histoConfigDict[FitObservable]["rebin"]
    configHistosProcs["range"] = histoConfigDict[FitObservable]["post_range"]
    if not configHistosProcs["type"] == "TH2D" and not configHistosProcs["type"] == "TH2F"  and not configHistosProcs["useBinvec"] == 0:
        configHistosProcs["useBinvec"] = 1

        binarray = array('d')
        [binarray.append(x) for x in configHistosProcs["binvec"]]
        configHistosProcs["BinArray"] = binarray
    else:
        configHistosProcs["useBinvec"] = 0

    for dsid in DsidList:
        hist = HistoPrep.Retrieve1DHisto_Signal( FitObservable , dsid,configHistosProcs  )
        DictHistSignals [dsid] = hist
    #FitObs_SMSig_orig = HistoPrep.Retrieve1DHisto_Signal( FitObservable, 7181 ,configHistosProcs )
    FitObs_SMSig = HistoPrep.Retrieve1DHisto_Signal( FitObservable, 7181 ,configHistosProcs )


    verbose = False
    TmplFit             = TemplateFit(HistoPrep.histo_BG_fit , FitObs_SMSig , DictHistSignals,TemplateInfo, randgen, ThrowToys )
    FitObservable       = runConfig["Fit_Observable"]


    zhh_dict = {}
    confPath = runConfig["ConfigFolder"]
    zhh_dict["zhh_file"]      = confPath + runConfig["Fit_Zhh_file"]
    zhh_dict["lumi"]          = 2500 #fixed lumi at stage 2
    zhh_dict["polarization"]  = 1.072

    Chi2dict, errdict       = TmplFit.GetChisquaresOfHistos(FitObservable,runConfig["Fit_Include_Zhh14"],zhh_dict, False)

    canv1               = Plotter.createCanvas()

    coupllist  = []
    chi2list   = []

    print("Chi2 for the SM sample {}".format( Chi2dict["7181"]))
    print("Chi2 for the (1.05,1) point {}".format( Chi2dict["8195"]))
    smallestchi2 =  float(Chi2dict["8195"])
    
    for dsid in Chi2dict:
        gHHH       = float( TemplateInfo.gHHH_from_DSID (dsid)  )
        gHHWW      = float( TemplateInfo.gHHWW_from_DSID(dsid)  )
        coupllist.append( [gHHH, gHHWW] )
        if verbose:
            print( "DSID={}, gHHH={}, gHHWW={}, chi2={:<2f}".format(dsid,gHHH, gHHWW, Chi2dict[dsid]))

    
    chi2graph2D = TGraph2D( len(coupllist) )

    if IsUseGraph:
        for i in xrange(len(coupllist)):
            gHHH   = coupllist[i][0]
            gHHWW  = coupllist[i][1]
            chi2   = Chi2_from_gHHH_gHHWW(coupllist[i][0], coupllist[i][1] , TemplateInfo, Chi2dict)
            if verbose:
                print("Setting this point gHHH={}, gHHWW={} to chi2 = {} ".format( gHHH, gHHWW, chi2  ))
            chi2graph2D.SetPoint(i, gHHH, gHHWW,chi2  )
            
        return chi2graph2D,smallestchi2


    #if not using the graph/interpolation at all, just fill the histogram strictly correctly
    chi2hist2D = TH2D("chi2_2D","chi2_2D", 500,0.2,2.6,100,0.6,1.4)
    for i in xrange(len(coupllist)):
        gHHH   = coupllist[i][0]
        gHHWW  = coupllist[i][1]
        chi2   = Chi2_from_gHHH_gHHWW(coupllist[i][0], coupllist[i][1] , TemplateInfo, Chi2dict)
        chi2hist2D.Fill(gHHH,gHHWW,chi2 )


    return chi2hist2D,smallestchi2

def GetChi2HistFromGraph2D( chi2graph2D,smallestchi2,histoConfig):
    
    chi2hist2D = TH2D("chi2_2D","chi2_2D", 500,0.2,2.6,100,0.6,1.4)
    #fill this histogram from the graph    
    chi2graph2D.Draw()
    print(" smallestchi2 = {}".format(smallestchi2))

    for xbin in xrange(chi2hist2D.GetNbinsX()+1):
        for ybin in xrange(chi2hist2D.GetNbinsY()+1):
            x=chi2hist2D.GetXaxis().GetBinCenter(xbin)
            y=chi2hist2D.GetYaxis().GetBinCenter(ybin)
            z =chi2graph2D.Interpolate(  x, y )  #is this actually correct?
            
            chi2hist2D.Fill(x,y,z-smallestchi2)
        
#    #this is filling directly a TH2D
#    chi2hist2D = TH2D("chi2_2D","chi2_2D", 50,0.1,2.8,50,0.5,1.5)
#    for i in coupllist:
#        chi2hist2D.Fill(i[0],i[1],  Chi2_from_gHHH_gHHWW(i[0], i[1] , TemplateInfo, Chi2dict)  )
    #chi2hist2D.Draw("HIST")

    chi2hist2D.Draw("HIST")
    xaxis = chi2hist2D.GetXaxis()
    yaxis = chi2hist2D.GetYaxis()
    if "xlimits" in histoConfig:
        xaxis.SetRangeUser( histoConfig["xlimits"][0],histoConfig["xlimits"][1])
    if "ylimits" in histoConfig:
        yaxis.SetRangeUser( histoConfig["ylimits"][0],histoConfig["ylimits"][1])


    return    chi2hist2D     



def FixedParabolas(  Chi2Graphtofit , canv , fitrange, fitfunction, IsFixToZero = False  ):
    if IsFixToZero:
        print("Fitting a parabola whose minimum is fixed to (1,0)")
        parabolic_fit  = TF1( "parabola", "[0]*x**2 -2 * [0]*x + [0]", fitrange[0], fitrange[1])
    else:
        print("Fitting a parabola whose minimum is fixed to xmin=1 and free ymin")
        parabolic_fit  = TF1( "parabola", "[0]*x**2 -2 * [0]*x + [1]", fitrange[0], fitrange[1])

    Chi2Graphtofit.Fit("parabola","R") #R: using the fit range
    #parabolic_fit.Draw("same")
    
    par0 = parabolic_fit.GetParameters()[0]
    if len(parabolic_fit.GetParameters())>1:
        par1 = parabolic_fit.GetParameters()[1]
    
    canv.Modified()
    canv.Update()

    yminval = parabolic_fit.GetMinimum()
    xmin    = parabolic_fit.GetX(yminval)
    #yminval = parabolic_fit.Eval(1.0) #since the minimum is fixed to x=1 in both cases (hardcoded here!)

    print("Minimum at xmin={:.3f}, ymin={:.3f}".format(xmin,yminval))
    if IsFixToZero:
        x2 = parabolic_fit.GetX( 1.0   )
        x1 = 1- (x2-1 )
        print("1 sigma limits from parabolic fit with function {}: {:.3f}, {:.3f}".format(parabolic_fit.GetExpFormula(),x1,x2))
    
    if not IsFixToZero:
        new_parabola = TF1( "shifted_parabola", "[0]*x**2 -2* [0]*x + [1]"    )#fix minimum to 1, but no ymin=0 fix
        new_parabola.FixParameter(0,par0)
        new_parabola.FixParameter(1,par1-yminval)#this shifts the parabola such that ymin=0

        x1 = new_parabola.GetX( 1.0   )
        x2 = 1- (x1-1 )
        print("1 sigma limits from shifting the parabola to ymin=0 : {:.3f}, {:.3f}".format(x1,x2))

    #with factor 1.8 to estimate the impact of the polarization
    pol_parabola =  TF1( "polarization_parabola", "[0]*x**2 -2* [0]*x + [0]"    )
    pol_parabola.FixParameter(0,1.8*par0)
    x1 = pol_parabola.GetX( 1.0   )
    x2 = 1- (x1-1 )
    print("1 sigma limits with factor 1.8 for signal, as a rough estimate: {:.3f}, {:.3f}".format(x1,x2))

    return

def fitfunction_notrestrictions(x,par):
        
    #free_fit  = TF1( "free_parabola", "[0]*x**2 +[1]*x + [2]", fitrange[0], fitrange[1])
    #parabola = par[0]*x[0]**2+par[1]*x[0] + par[2] 
    #return parabola
    #do not forget to adjust the number of parameters in free_fit!
    if x[0] < par[0]:
        parabola= par[1]*x[0]**2 + par[2]*x[0] + par[3]
    else:
        parabola = par[4]*x[0]**2 + par[5]*x[0] + par[6]
    return parabola

def fitfunction_notminimumbutsamep0(x,par):
        
    #free_fit  = TF1( "free_parabola", "[0]*x**2 +[1]*x + [2]", fitrange[0], fitrange[1])
    #parabola = par[0]*x[0]**2+par[1]*x[0] + par[2] 
    #return parabola
    #do not forget to adjust the number of parameters in free_fit!
    if x[0] < par[0]:
        parabola= par[1]*x[0]**2 + par[2]*x[0] + par[3]
    else:
        parabola = par[4]*x[0]**2 + par[5]*x[0] + (par[1]-par[4])*par[0]**2 + (par[2]-par[5])*par[0] + par[3]
    return parabola


def fitfunction_parabola(x,par):#_parabola

    """The regular _parabola, which is highly fitrange dependent"""
    parabola = par[0]*x[0]**2+par[1]*x[0] + par[2] 
    return parabola
    
def fitfunction_2sidedparabola(x,par):#
    """_2sidedparabola"""
    #free_fit  = TF1( "free_parabola", "[0]*x**2 +[1]*x + [2]", fitrange[0], fitrange[1])
    #parabola = par[0]*x[0]**2+par[1]*x[0] + par[2] 
    #return parabola
    #do not forget to adjust the number of parameters in free_fit!

    #a = 1 - exp ( - par[4] * ( x[0] - par[0])**2)
    if x[0] < par[0]:
        #parabola = par[1]*x[0]**2 - 2* par[1]*par[0]*x[0] + par[2]
        parabola = par[1] * (x[0]- par[0])**2 + par[3]
        #parabola = a* parabola
    else:
        #parabola = par[3]*x[0]**2 - 2* par[3]*par[0]*x[0] + (par[3]-par[1])* par[0]**2 +par [2]
        parabola = par[2] * (x[0]- par[0])**2 + par[3]
        
        #parabola = a* parabola
    return parabola

def fitfunction(x,par):
    """This is the final and best fitfunction: a polynomial of 4th order _pol4"""

    polynomial = par[0]*x[0]**4 + par[1]*x[0]**3 + par[2]*x[0]**2 + par[3]*x[0] + par[4]
    
    return polynomial


def fitfunction_higherpolynomial(x, par):
    #fit = par[0]*x[0]**4 + par[1]*x[0]**3 + par[2]*x[0]**2 + par[3]*x[0] + par[ 4]
    fit = par[1]*(x[0]-par[0])**4 + par[2]*( x[0] - par[0])**3+ par[3]*( x[0] - par[0])**2+  par[4]*( x[0] - par[0])
    return fit
    
def ParabolicFit( Chi2Graphtofit , canv , fitrange, fitfunction, IsFixToZero = False, InitialValueMinimum = 1.0):
    
    print("\n\nParabolic Fit")
    #FixedParabolas(  Chi2Graphtofit , canv , fitrange, fitfunction, IsFixToZero  )
    print("Fitting range for the parabola(s): {}".format(fitrange))

    #as a comparison to test the stability of the fit, also use a completely free-floating parabola!
    #free_fit  = TF1( "free_parabola", "[0]*x**2 +[1]*x + [2]", fitrange[0], fitrange[1])
    free_fit  = TF1( "free_parabola", fitfunction, fitrange[0], fitrange[1],5) #last argument is the number of parameters

    #if InitialValueMinimum > 1.3:
    #    InitialValueMinimum = 1.3
    #print("Starting point for minimum in the fit: {}".format(InitialValueMinimum))

    free_fit.SetParameter(0,InitialValueMinimum)
    #free_fit.SetParLimits(0,InitialValueMinimum-0.4, InitialValueMinimum+0.4)
    free_fit.SetParameter(1,30)
    free_fit.SetParameter(2,30)
    free_fit.SetParameter(3,10)

    ################free_fit.FixParameter(0,1)
    fitresult =    Chi2Graphtofit.Fit("free_parabola","R") #R: using the fit range, S: creating the fitresult pointer
    free_fit.SetLineColor(kGreen)
    free_fit.Draw("same")
    canv.Modified()
    canv.Update()

    pars = free_fit.GetParameters() # a list of the parameters

    free_fit.SetLineColor(kGreen)
    free_fit.Draw("same")
    canv.Modified()
    canv.Update()
    #free_yminval = free_fit.GetMinimum()
    #free_xmin    = free_fit.GetX(free_yminval)

    ##gradually make the interval smaller from the right until a minimumpoint is found (worst case at 0 when I know there is a problem!)
    #


    #CHECK FOR A MINIMUM FURTHER LEFT!
    #free_xmin    = free_fit.GetMinimumX(0.49,2.5)
    #free_xmin, isTrueMinimum = MinimumChecker(free_fit,  pars, 0.49, 2.5)
    free_xmin, isTrueMinimum = MinimumChecker(free_fit,  pars, fitrange[0],fitrange[1])
    free_yminval = free_fit.Eval(free_xmin)
    
    x1 = free_fit.GetX(free_yminval+ 1.0 ,0, free_xmin  )
    x2 = free_fit.GetX(free_yminval+ 1.0 , free_xmin ,2.5 )
    print("Minimum of the free_fit parabola at xmin={:.3f}, ymin={} ".format(free_xmin,free_yminval))
    print("1sigma read off from the free_fit parabola (after correcting for the y_min value): {}, {} (CAVEAT might be wrong if there is a second minimum)".format(x1,x2))

    ParabFitDict = {}
    ParabFitDict ["xmin_free_parabola"] = free_xmin
    
    ParabFitDict["is_minimum"] = isTrueMinimum


    ParabFitDict["status"] = int(fitresult)
    if int(fitresult) == 4:
        print("There was a problem with the fit, status {}" .format(int(fitresult)))

    print("xmin: {}".format(free_xmin))

    return ParabFitDict 

def gausfcn(x, par):
    if x[0]< par[0]:
        gausf = par[1] * TMath.Gaus(x[0], par[0], par[2])
    else: 
        gausf = par[1]* TMath.Gaus(x[0], par[0], par[3])
    return gausf


def MinimumChecker(function,  pars, xlow, xup ,verbose = False ):
    xmin = function.GetMinimumX(xlow,xup)
    isTrueMinimum = IsMinimum(function, xmin, pars)
    if not isTrueMinimum:
        #this means there is no minimum to be found, so the point should not be used at all
        return xmin, False
    #even if it is a true minimum, there could be a true minimum further left
    xup = xmin
    IsAnotherMinimum = False
    while xup > xlow+0.1:
        xup -= 0.1
        if verbose:
            print("Finding minimum between {} and {}".format(xlow,xup))
        xmin_new = function.GetMinimumX(xlow,xup)
        if IsMinimum(function, xmin_new, pars):
            print("Found the left-most minimum at xmin = {} which is at lower x than the other minimum at xmin = {}".format(xmin_new, xmin))

                        
            return xmin_new, True


    #if no other minimum was found between the first one found and xlow, just return the original one
    return xmin, isTrueMinimum




def IsMinimum(function, xpoint,pars,verbose = False):
    """returns true if the point is a minimum"""
    isMin = False
    derivative = function.Derivative(xpoint,pars)
    if verbose:
        print("derivative : {} at x = {}".format(derivative, xpoint))
    if abs(derivative) < 0.0001:
        isMin = True
    return isMin

def ToyExp1D(  runConfig, TemplateInfo, histoConfigDict, HistoPrep, DsidList1D, randgen , gausfcn, Plotting, zhh_dict ,HHvvStg2_dict):
    gROOT.SetBatch(True)
    #gROOT.SetBatch(False)

    nfailed = 0
    confPath = runConfig["ConfigFolder"]

    ntoys=runConfig["Fit_ntoys_1D"]
    fitrange = runConfig["Fit_fitrange_estimategHHH"]
    meanlist = []
    for toy in xrange(ntoys):
        #raw_input()
        print("{}th toy".format(toy))
        Chi2Graph1Dfit_toy, canv = FillChi2Graph( TemplateInfo, confPath, runConfig, histoConfigDict, HistoPrep, DsidList1D, randgen ,zhh_dict,HHvvStg2_dict, ThrowToys=True)
        #make a parabolic fit and get its minimum

        #get the minima from the minima of the tgraphs:
        chi2graph_array_x_orig = Chi2Graph1Dfit_toy.GetX()
        chi2graph_array_y_orig = Chi2Graph1Dfit_toy.GetY()

        chi2graph_array_x = array('d')
        chi2graph_array_y = array('d')
        for i in xrange(23):
            chi2graph_array_x.append(chi2graph_array_x_orig[i])
            chi2graph_array_y.append(chi2graph_array_y_orig[i])

        
        
        toy_minimum_y = min( chi2graph_array_y )
        for n in xrange(len(chi2graph_array_x)):
            if chi2graph_array_y[n] == toy_minimum_y:
                toy_minimum_x = chi2graph_array_x[n]
                #print(toy_minimum_x)
        #meanlist.append(toy_minimum_x)
        #print("minimum is {}".format(toy_minimum_x   ))

        fitrange = [toy_minimum_x-0.4, toy_minimum_x+0.4  ]
        fitrange = [0.49, toy_minimum_x+0.4  ]
        fitrange = [0.49, 2.49  ]

        
        #get the mean from the parabolic fit:

        FitDict_toy = ParabolicFit ( Chi2Graph1Dfit_toy, canv,fitrange,fitfunction,  False , InitialValueMinimum = toy_minimum_x)

        if not FitDict_toy["status"] == 4  and FitDict_toy["is_minimum"]:
            meanlist.append(FitDict_toy["xmin_free_parabola"])
        else:
            print("Not using the fit result as it has not converged or is not a minimum! ")
            #print("Instead using the minimum of the toys: {}".format(toy_minimum_x))
            #meanlist.append(toy_minimum_x)
            nfailed +=1
        #meanlist.append(toy_minimum_x)


    if len(meanlist) == 0:
        return
    print("This is the list of estimates for gHHH: {}".format(meanlist))
    print("A total of {} toys has failed and was not counted.".format(nfailed))
    gROOT.SetBatch(False)
    canv2 = Plotter.createCanvas()
    
    #binvec_gauss = [0.2, 0.3, 0.45, 0.525, 0.575, 0.625, 0.675, 0.725, 0.775, 0.825, 0.875, 0.925, 0.975, 1.025, 1.075, 1.125, 1.175, 1.225, 1.275, 1.325, 1.375, 1.425, 1.475 , 1.5]
    #binarray_gauss = array('d')
    #[binarray_gauss.append(x) for x in binvec_gauss]
    #gaushist = TH1F("gaushist","gaushist",len(binarray_gauss)-1, binarray_gauss)

    gaushist = TH1F("gaushist","gaushist",100,0,2)
    for m in meanlist:
        gaushist.Fill(m)

    gaushist.SetStats(0)
    gausConfig = {"Title": "", "xTitle" : "g_{HHH}/g^{SM}_{HHH}", "yTitle": "", "lumi" : runConfig["Lumi"] , "polarization" : runConfig["Polarization_factor_SIG"] }

    canv2, gaushist, t1 ,lumit= Plotting.SetGeneralStyle(canv2, gaushist, gausConfig)
    gausfit = TF1("gausfcn", gausfcn, 0.5, 2.5,4 )
    gausfit.SetParameter(0,1)
    gausfit.SetParameter(1,1)
    gausfit.SetParameter(2,0.1)
    gausfit.SetParameter(3,0.1)

    fitresults = gaushist.Fit(gausfit, "S")
    raw_input("Enter to proceed with results")
    chi2 = fitresults.Chi2()
    ndf = fitresults.Ndf()
    print("Chi2: {}, NDF: {}, Chi2/NDF: {}".format(chi2,ndf,chi2/ndf))
    canv2, gaushist, t1 ,lumit= Plotting.SetGeneralStyle(canv2, gaushist, gausConfig)
    canv2.Modified()
    canv2.Update()
    SaveCanvas( canv2, gaushist, "GaussOfMinima.pdf" )
    return



def PlotChi2_2D(TemplateInfo,runConfig,histoConfigDict, HistoPrep,randgen , Plotting ):

    DsidList2D                 = TemplateInfo.DsidList2D()
    histoConfig_chi22D = { "Title": "#Delta #chi^{2}",
                           "xTitle": "g_{HHH}/g_{HHH}^{SM}", "yTitle": "g_{HHWW}/g_{HHWW}^{SM}", "lumi":  runConfig["Lumi"], "xlimits": [0.45,1.55], "ylimits": [0.89,1.11],  "polarization":  runConfig["Polarization_factor_SIG"]}
    print("Working on the 2D chi-square ....")



    chisquaregraph,smallestchi2 = FillChi2Graph2D(  TemplateInfo, runConfig, histoConfigDict, HistoPrep, DsidList2D  ,randgen, ThrowToys=False, IsUseGraph=True)
    canv, chisquaregraph = Plotting.Plot2D(  chisquaregraph,histoConfig_chi22D,runConfig["Fit_Include_Zhh14"])
    raw_input("Converting now the graph to hist: press Enter")
    canv.cd()
    gStyle.SetNumberContours(150)

    chisquarehisto = GetChi2HistFromGraph2D(chisquaregraph,smallestchi2,histoConfig_chi22D)
    
    contourschi2 = [ 2.3,6.18]#,11.83] #2sigma: 6.18
    #contours = [1.0]
    if contourschi2[0]==1.0:
        print("WARNING WARNING WARNING WARNING using contour level chi2=1 instead of 2.3. Only allowed exceptionally. WARNING WARNING WARNING WARNING")
    chisquarecont = chisquarehisto.Clone()
    contourarray = array('d')
    [contourarray.append(x) for x in contourschi2]
    chisquarecont.SetContour(len(contourschi2),contourarray)

    chisquarecont.SetStats(0)
    chisquarehisto.SetStats(0)
    #create the contours objects
    chisquarecont.Draw("CONTZ LIST")
    
    canv.Update()
    
    gStyle.SetNumberContours(150)
    
    chisquarehisto.Draw("colz")
    chisquarecont.Draw("CONT2 same")
    configHistos = {"Title": "", "xTitle" : "g_{HHH}/g_{HHH}^{SM}", "yTitle": "g_{HHWW}/g_{HHWW}^{SM}", "lumi" : runConfig["Lumi"],  "polarization":  runConfig["Polarization_factor_SIG"]  }
    canv.SetGridy()
    canv.SetGridx()
    canv.Modified()
    canv.Update()
    
    canv, chisquarehisto, t1,lumit = Plotting.SetGeneralStyle(canv, chisquarehisto, configHistos)
    contours = gROOT.GetListOfSpecials().FindObject("contours")
    if not contours:
        print("There is a problem, did not find the contours object. Make sure to plot with CONTZ LIST and to do canvas.Update() before looking for the contours in gROOT.GetListOfSpecials()")
    else:
        print("Found contours object {} of type {}".format(contours,type(contours)))
        contgrlist = []
        for contourindex in xrange(len(contours)):
            canv, contgr = DrawContour(canv,contours,contourindex,runConfig["Fit_Observable"])
            contgrlist.append(contgr)
        #contlist = TList(contours.At(0))
        
        canv1, chisquaregraph = Plotting.Plot2D(  chisquaregraph,histoConfig_chi22D,runConfig["Fit_Include_Zhh14"])

        canv1.cd()

        chisquaregraph.SetDrawOption("")
        chisquaregraph.SetMarkerSize(0)
        chisquaregraph.SetMarkerColor(0)
        canv1.SetGridx()
        canv1.SetGridy()
        chisquaregraph.SetTitle("")
        contleg = TLegend(0.6,0.7,0.85,0.8)
        contleg.SetFillStyle(0)
        contleg.SetBorderSize(0)
        a=0
        for contgri in contgrlist:
            label = SetChi2Label(contourschi2[a])
            contleg.AddEntry( contgri, "{}".format(label), "p")
            contgri.SetLineColor(kAzure+3-6*a)
            contgri.SetLineWidth(6)
            contgri.SetMarkerStyle(21)
            contgri.SetMarkerColor(kAzure+3-6*a)
            contgri.SetMarkerSize(0.8)
            contgri.Draw("l")

            #canv.Modified()
            #canv.Update()
            canv1.Modified()
            canv1.Update()
            a+=1
        contleg.Draw()
        #canv, contgr, t1,lumit = Plotting.SetGeneralStyle(canv, contgr, configHistos)            
        canv1.cd()

    canv1, g, t1 ,lumit = Plotting.SetGeneralStyleChi2(canv1,chisquaregraph,configHistos, runConfig["Fit_Include_Zhh14"], runConfig["Fit_extratext"])
    canv1.Modified()
    canv1.Update()    
    print dir(lumit[0]), t1, lumit[0].GetTitle()
    opttext1 = "HH#nu#nu: 3 TeV; 5 ab^{-1}"
    lumit[0].DrawLatex(0.6,0.9,opttext1)
    opttext2 = " + ZHH: 1.4 TeV; 2.5 ab^{-1}"
    if runConfig["Fit_Include_Zhh14"]:
        lumit[0].DrawLatex(0.8,0.9,opttext2)


    lumit[1].DrawLatex(0.6,0.93,runConfig["Fit_extratext"])
        
    g.GetXaxis().SetTitleOffset(1.)
    g.GetXaxis().SetTitleSize(0.07)
    g.GetYaxis().SetTitleOffset(1.3)
    g.GetYaxis().SetTitleSize(0.07)
    SaveCanvas(canv1, chisquarehisto, "chi2_2D.pdf")
    return canv, chisquarehisto

def SetChi2Label(chi2value):
    if chi2value==2.3:
        CL = "68% C.L."
    elif chi2value==6.18:
        CL = "95% C.L."
    else:
        CL = "#chi{} ={}".format("^{2}",chi2value)
    return CL
def DrawContour(canv,contours,contourindex,fitObservable):
    contlist = contours.At(contourindex)
    print("Drawing contour number {} based on this object {}".format( len(contlist), contlist))
        
    
    contgr = TGraph()
    contgr.Merge(contlist)
    #canv, contgr, t1,lumit = Plotting.SetGeneralStyle(canv, contgr, configHistos)            
    canv.Modified()
    canv.Update()
    contgr.SetName("contour_1sigma")
    save_it_as  = raw_input("How to call the binning used for this contour? Dont save: Enter, else it will be called: Optimize2D/Contour2D_{}_<binning>.root : ".format(fitObservable))
    

    if save_it_as:
        contgr.SaveAs("Optimize2D/Contour2D_{}_{}.root".format( fitObservable, save_it_as  ))
    return canv, contgr
    
def main(*args):

    if len(args) > 1:
        runConfigFile = args[1]
    else:
        sys.exit("Please provide a run configuration file as argument, e.g. ../Configs/runConfig_2016.json")
        
    print("Using this config file: {}".format(runConfigFile))
    gStyle.SetLineWidth(2)
    gStyle.SetLabelSize(12)
    # ====== Prepare all the infos: histos and configs  ==================================================
    runConfig = ReaderTools.configDictFromJson(runConfigFile)
    confPath = runConfig["ConfigFolder"]

    samplesDict_Sig = dictAnaSample_FromCSV(confPath + runConfig["Run_Sig_DsidCsvFile"])
    groupDict_BG= ReaderTools.configDictFromJson(confPath + runConfig[ "Proc_BG_Group_Config"])


    zhh_dict = {}
    zhh_dict["zhh_file"]      = confPath + runConfig["Fit_Zhh_file"]
    zhh_dict["lumi"]          = 2500 #fixed lumi at stage 2
    zhh_dict["polarization"]  = 1.072
    #for zhh, also lumi and polarization need to be passed on
    HHvvStg2_dict = {}
    HHvvStg2_dict["polarization_factor"]  = runConfig["Polarization_factor_SIG"]
    HHvvStg2_dict["lumi"]       = 2500

    histosSignal = {}
    histosSignal = ReadDatasetHistos(histosSignal,runConfig["Run_overallTag"]+"/"+runConfig[ "Fit_Sig_histoFile_wgt"]  ,samplesDict_Sig ).Histograms
    histosBG = {}
    histosBG = ReadDatasetHistos(histosBG, runConfig["Run_overallTag"]+"/"+runConfig[ "Fit_BG_histoFile_wgt"], groupDict_BG).Histograms
    SigGroupDict = ReaderTools.configDictFromJson(confPath + runConfig[ "Proc_Sig_Group_Config"])
    legenddict = ReaderTools.configDictFromJson(confPath + runConfig["Plot_Legend_BG"])

    histoConfigDict = ReaderTools.configDictFromJson(confPath + runConfig["Run_Histo_Config"])

    # ====== Create the TemplateObjects instance that knows all the histos and configs ========================
    TemplateInfo = TemplateObjects( samplesDict_Sig ,SigGroupDict, histosBG , histosSignal, histoConfigDict)
     



    # ====== Retrieve the HistoPrepare object - individual histos can be requested ===========================
    HistoPrep = HistoPrepare(TemplateInfo)
    #print    HistoPrep.TemplateObjects.histos_Sig["7163"][4].GetXaxis().GetBinLabel(1)


    # ====== Instanciate a PlotHistos instance to make use of for plotting functions =========================
    Plotting = PlotHistos(runConfig)

    #print    HistoPrep.TemplateObjects.histos_Sig["7163"][4].GetXaxis().GetBinLabel(1)
    #here the 1Dvs1D histograms still have there bin labels! where do they loose them?


    # ====== Plot the 2D cross section and event yield dependence on couplings ==============================
    if runConfig["Plot_2DHistos"]:
        Plot2DHistos( HistoPrep, Plotting, runConfig )



    
        

    # ====== Plot the SM 1D distributions ====================================================================
    # since the template fit relies on these histogram objects, this always has to run (could do this differently though)
    #histnames = ["BDT", "SumMj"]
    histnames =[runConfig["Fit_Observable"]]
    Plot1DSMdistributions (  histoConfigDict, runConfig, confPath, HistoPrep, Plotting ,histnames)

    # ====== Plot BG plus a list of signals, 1D distributions ================================================
    PlotBGPlusListOfSignals1D( histoConfigDict, runConfig, confPath, HistoPrep, Plotting , TemplateInfo,histnames   )

    gStyle.SetLegendTextSize(0.045)
    PlotSignalsOnly1D(  histoConfigDict, runConfig, confPath, HistoPrep, Plotting , TemplateInfo,histnames )

    randgen = TRandom3()


    # ====== prepare for 1D fit
    # ====== retrieve all the gHHWW = 1 histograms and add them to a dictionary (why?)
    DsidList1D                 = TemplateInfo.DsidList1D()

    #plot xsec 1D only on gHHWW=1 line
    #not necessary for the next steps, but a nice addition
    if runConfig["Plot_xsecs_gHHH"]:
        Plot1DgHHHxsec_HHnunu3TeV( runConfig, HistoPrep, Plotting, DsidList1D )
        Plot1DgHHHxsec_Zhh1400GeV( confPath, runConfig, Plotting )

        
    # ====== nominal fit without any toys
    Chi2Graph1Dfit, canv = FillChi2Graph( TemplateInfo, confPath, runConfig, histoConfigDict, HistoPrep, DsidList1D ,randgen, zhh_dict,HHvvStg2_dict, ThrowToys=False)
    FitDict = ParabolicFit ( Chi2Graph1Dfit, canv,runConfig["Fit_fitrange_estimategHHH"],fitfunction, False )
    
    SaveCanvas( canv, Chi2Graph1Dfit, "chi2.pdf" )




    # ====== 2D  chi2 plot
    canv1, chisquarehisto = PlotChi2_2D(TemplateInfo,runConfig,histoConfigDict, HistoPrep,randgen, Plotting  )
    




    # ===== throw toys (1D): vary the SM histogram acc. to Poissonian, and for each of those fill the chi2graph => get many chi2graphs from which the error bars can be constructed, maybe?? but then do I need toys for that if they are just Poissonian error bars?
    # then, get many FillChi2Graph s for the variations... how exactly?

    
    if "n"==raw_input("Start Toys? [Y/n]"):
        sys.exit("thanks")
    ToyExp1D(runConfig, TemplateInfo, histoConfigDict, HistoPrep, DsidList1D, randgen,gausfcn,Plotting, zhh_dict,HHvvStg2_dict)

if __name__ == "__main__":
    main(*sys.argv)
