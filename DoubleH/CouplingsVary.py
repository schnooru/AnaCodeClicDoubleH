from ROOT import TH2F, THStack, gPad , TH1F

class CouplingsVary:
    """contains info about the varied couplings samples: event yields, cross sections, etc
    and facilities to create histograms from them
    """

    def __init__(self, SignalCSVdict, SignalGroupDict, wgtdBackgroundHistos, wgtdSignalHistos, histoConfigDict):
        #background histograms are already weighted, so maybe do not need the CSV any more.
        #for signal in case of signal-only plots, need CSV with DSIDs to know the couplings

        self.histos_Sig = wgtdSignalHistos
        self.histos_BG = wgtdBackgroundHistos
        
        self.histoConfigDict = histoConfigDict
        self.SigCouplingsDict = SignalCSVdict
        self.SigGroupDict = SignalGroupDict

        return


    def CrossSectionHistogram(self, canv):
        """Creates 2D histo with [averaged] cross sections from csv"""
        print("CrossSectionHistogram: create 2D histogram of cross sections")
        histo = TH2F("xsec","xsec",50,0.1,2.8,50,0.5,1.5)
        for i in self.SigCouplingsDict.keys():
            gHHH      = float( self.SigCouplingsDict[i]["couplings"][0]  )
            gHHWW     = float( self.SigCouplingsDict[i]["couplings"][1]  )
            xsec      = float( self.SigCouplingsDict[i]["xsec"]          )
            
            averager_binlist = [] #for iterative mean: stores number of entries
            #if bin content previously empty, just do:
            if histo.GetBinContent( histo.FindBin(gHHH, gHHWW)) == 0:#histo.GetBin( gHH, gHHW  )
                histo.Fill( gHHH, gHHWW, xsec)
            #if not, fill with the average of the new and the old one (OK if never more than two samples which is not the case.)
            else:
                fillbin = histo.FindBin(gHHH, gHHWW)
                averager_binlist.append(fillbin)
                
                n_new = averager_binlist.count(fillbin)
                # iterative mean: c(t+1) = c(t)+1/(t+1) (x - c(t)) x...new value, c(t)...old average
                oldvalue = histo.GetBinContent(fillbin )
                newvalue = oldvalue + 1./(n_new)*( xsec - oldvalue )
                
                #print gHHH, gHHWW, oldvalue, newvalue
                histo.SetBinContent( histo.FindBin(gHHH, gHHWW), newvalue) 

        return histo, canv


    def EventYieldHisto(self):
        """go through the histograms in self.histos_Sig , get the number of events"""
        
        histo = TH2F("event yields","event yields",100,0.1,2.8,80,0.5,1.5)
        #print self.histos_Sig, self.SigCouplingsDict

        #need to apply the grouping! otherwise it is filled again.

        for DSID in self.SigCouplingsDict:
            gHHH      = float( self.SigCouplingsDict[DSID]["couplings"][0]  )
            gHHWW     = float( self.SigCouplingsDict[DSID]["couplings"][1]  )

            evyield_DSID = 0
            
            if not DSID in self.SigGroupDict.keys():
                #print("not filling for {} with couplings {},{}".format(DSID, gHHH, gHHWW))
                continue
            for h in self.histos_Sig["{}".format(DSID)]:
                if h.GetName() == "SumMj_{}".format(DSID):
                    evyield_DSID = h.Integral()
            
            histo.Fill( gHHH, gHHWW, evyield_DSID  )
        return histo

    def Retrieve1DHisto_Signal(self, histname, dsid, rebin=1 ):

        dsid=str(dsid)
        if not dsid in self.histos_Sig:
                print("Problem: looking for the name {} in the histos, but it does not exist.".format(dsid))
        for h in self.histos_Sig[str(dsid)]:
            if h.GetName().startswith(histname):
                h.Rebin(rebin)
                return h.Clone()
        print("ERROR was not able to retrieve 1D histo for the signal")
        return 

    def Stack1DHisto(self,hstack,histname,legend, ordered_procnames, procdict, rebin=1):
        #hstack = THStack(histname, histname)
        i = 0
        #isFirstProc = True
        for dsid in ordered_procnames:
            i+=1
            if not dsid in self.histos_BG:
                print("Problem: looking for the name {} in the histos, but it does not exist.".format(dsid))
                continue
            for h in self.histos_BG[str(dsid)]:
                if h.GetName().startswith(histname):
                    h.SetLineColor(12)
                    h.SetLineWidth(2)
                    h.SetFillColor(procdict[dsid]["color"])
                    hi = h.Clone()
                    hi.Rebin(rebin)
                    #print("rebinning, proc: {}".format(dsid))
                    hstack.Add(hi)
        hlist = hstack.GetHists()
        for h in reversed(hlist):
            legend.AddEntry(   h,procdict[h.GetName().split("_")[1]]["name"],"lf")
        self.TotalBGHisto = self.GetTotalBGHist(hstack)
        return hstack, legend

    def GetTotalBGHist(self, hstack):
        stack = hstack.Clone()
        histlist = stack.GetHists()
        hist1 = histlist[0]

        histo = TH1F(stack.GetName(), stack.GetName(), hist1.GetNbinsX(), hist1.GetBinLowEdge(1), hist1.GetBinLowEdge(hist1.GetNbinsX()+1  ))
        


        for hi in histlist:
            histo.Add(hi)
        return histo