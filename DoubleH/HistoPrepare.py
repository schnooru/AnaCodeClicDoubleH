from TemplateObjects import TemplateObjects
from ROOT import TH2F, TLegend, THStack,TH1F,gDirectory,TH1D, TH2D, TGraph
from clicpyana.PlottingTools import Plotter
from array import array
import sys
sys.path.append('/usr/lib64/python2.6/site-packages')
import numpy as np

class HistoPrepare:
    def __init__(self, TemplateObjects, verbose = False):
        self.TemplateObjects = TemplateObjects
        #self.histo_BG_fit
        self.histoObjectsAllSM = {}
        self.verbose           = verbose
        self.HasBGHistoForFit  = False
        return

    def SetBGHistoForFit(self, histogram):
        self.histo_BG_fit       = histogram.Clone()
        self.HasBGHistoForFit   = True
        return

    def CrossSectionHistogram2D(self):
        """Creates 2D histo with [averaged] cross sections from csv"""
        print("CrossSectionHistogram: create 2D histogram of cross sections")
        histo = TH2F("xsec","xsec",50,0.1,2.8,50,0.5,1.5)
        for i in self.TemplateObjects.SigCouplingsDict:
            gHHH      = float( self.TemplateObjects.gHHH_from_DSID ( i )  )
            gHHWW     = float( self.TemplateObjects.gHHWW_from_DSID( i )  )
            xsec      = float( self.TemplateObjects.xsec_from_DSID ( i )  )
            
            averager_binlist = [] #for iterative mean: stores number of entries
            #if bin content previously empty, just do:
            if histo.GetBinContent( histo.FindBin(gHHH, gHHWW)) == 0:
                histo.Fill( gHHH, gHHWW, xsec)
            #iterative mean    
            else:
                fillbin = histo.FindBin(gHHH, gHHWW)
                averager_binlist.append(fillbin)
                
                n_new = averager_binlist.count(fillbin)
                # iterative mean: c(t+1) = c(t)+1/(t+1) (x - c(t)) x...new value, c(t)...old average
                oldvalue = histo.GetBinContent(fillbin )
                newvalue = oldvalue + 1./(n_new)*( xsec - oldvalue )
                
                #print gHHH, gHHWW, oldvalue, newvalue
                histo.SetBinContent( histo.FindBin(gHHH, gHHWW), newvalue) 

        return histo

    def CrossSectionGraph1D(self,DsidList1D):
        """Creates 1D histo with [averaged] cross sections from csv for all gHHWW samples"""
        gHHHlist = []
        xseclist = []

        for i in DsidList1D:
            gHHH      = float( self.TemplateObjects.gHHH_from_DSID ( i )  )
            gHHHlist.append(gHHH)
            xsec      = float( self.TemplateObjects.xsec_from_DSID ( i )  )
            xseclist.append(xsec)
        gHHHarray = np.array(gHHHlist)
        xsecarray = np.array(xseclist)
        histo = TGraph(len(gHHHarray),gHHHarray, xsecarray)  

        return histo

    def EventYieldHisto(self):
        """go through the histograms in TemplateObjects.SigCouplingsDict , get the number of events"""
        
        histo = TH2F("event yields","event yields",100,0.1,2.8,80,0.5,1.5)

        #need to apply the grouping! otherwise it is filled again.

        for DSID in self.TemplateObjects.SigCouplingsDict:
            gHHH      = float( self.TemplateObjects.SigCouplingsDict[DSID]["couplings"][0]  )
            gHHWW     = float( self.TemplateObjects.SigCouplingsDict[DSID]["couplings"][1]  )

            evyield_DSID = 0
            
            if not DSID in self.TemplateObjects.SigGroupDict.keys():
                #print("not filling for {} with couplings {},{}".format(DSID, gHHH, gHHWW))
                continue
            for h in self.TemplateObjects.histos_Sig["{}".format(DSID)]:
                if h.GetName() == "SumMj_{}".format(DSID):
                    evyield_DSID = h.Integral()
            
            histo.Fill( gHHH, gHHWW, evyield_DSID  )
        return histo

        
    def Stack_SM_list( self ,  configHistosProcs ):
        print("HistoPrepare: Getting the stack for the following processes: {}\n.............................................".format( ", ".join(map(str,configHistosProcs["ordered_processes"]  ))))
        legend = TLegend(0.5,0.4,0.89,0.89)
        legend.SetBorderSize(0)
        legend.SetFillStyle(0)

        _canv = Plotter.createCanvas()
        histname   = configHistosProcs["histname"]
        procdict   = configHistosProcs["procdict"]
        rebin      = configHistosProcs["rebin"]
        #axisrange  = configHistosProcs["range"]
        # create the stack
        hstack = THStack(histname,histname)
        for proc in configHistosProcs["ordered_processes"]:
            if not proc in self.TemplateObjects.histos_BG:
                print("Problem: looking for the name {} in the histos, but it does not exist.".format(dsid))
                continue
            for h in self.TemplateObjects.histos_BG[str(proc)]:
                #if not h.GetName().startswith(histname):
                if not h.GetName().split("_")[0] == histname:
                    continue
                #gDirectory.ls()
                hist_orig = h.Clone()
                hist_orig.Draw()
                #print("what are the bin labels? {}".format(hist_orig.GetXaxis().GetBinLabel(1)))

                hi = self.HistRangeBinning(hist_orig.Clone(), configHistosProcs)

                #for i in xrange(1,hi.GetXaxis().GetLast()):
                #    print("what are the bin labels? {}".format(hi.GetXaxis().GetBinLabel(i)))

                hi.SetLineColor(12)
                if self.verbose:
                    print("proc {} with color {}".format(proc,  procdict[proc]["color"]  ))
                hi.SetFillColor( procdict[proc]["color"] )
                hi.SetLineWidth(2)
                if self.verbose:
                    print("xlow after HistRangeBinning: {}".format(hi.GetBinLowEdge( hi.GetXaxis().GetFirst() )))


                
                hi.Draw()
                #print("HistoPrepare::Stack_SM_list: Adding histo {} {}".format(hi.GetName(),hi.GetTitle()))
                hstack.Add(hi)
                

        _canv.Close()

        hlist = hstack.GetHists()
        for h in reversed(hlist):
            legend.AddEntry(   h,procdict[h.GetName().split("_")[1]]["name"],"lf")

        return hstack, legend


    def CreateLegend(self):
        legend = TLegend(0.46,0.6,0.86,0.86)
        legend.SetBorderSize(0)
        legend.SetFillStyle(0)
        return legend

    def Retrieve1DHisto_Signal(self, histname, dsid,configHistosProcs):
        rebin      = configHistosProcs["rebin"]
        #axisrange  = configHistosProcs["range"]

        dsid=str(dsid)
        if not dsid in self.TemplateObjects.histos_Sig:
                print("Problem: looking for the name {} in the histos, but it does not exist.".format(dsid))
        for h in self.TemplateObjects.histos_Sig[str(dsid)]:
            if h.GetName().startswith(histname):
                h = self.HistRangeBinning(h.Clone(), configHistosProcs)
                return h.Clone()
        print("ERROR was not able to retrieve 1D histo for the signal")
        return 
#   

    def GetTotalHistFromStackProcessList(self, hstack, configHistosProcs, processList):
        rebin      = configHistosProcs["rebin"]
        #axisrange  = configHistosProcs["range"]
        stack = hstack.Clone()
        histlist = stack.GetHists()
        hist1 = histlist[0]
        print("HistoPrepare: Original bin label of first bin: {}".format( hist1.GetXaxis().GetBinLabel( hist1.GetXaxis().GetFirst()  ) ))


        histo = hist1.Clone()
        histo.SetName(stack.GetName())
        for hi in histlist:
            print("HistoPrepare::GetTotalHistFromStack: Adding histo {} {}".format(hi.GetName(),hi.GetTitle()))

            if hist1 == hi:
                #print("no double counting {}".format(hi.GetName()))
                continue
            if not hi.GetName().split("_")[1]  in processList:
                print("HistoPrepare::GetTotalHistFromStack: Ignoring Histo {} {}".format(hi.GetName(),hi.GetTitle()))
                continue
            hi = self.HistRangeBinning(hi.Clone(), configHistosProcs)
            histo.Add(hi)
        return histo


    def HistRangeBinning(self, histo, configHistosProcs):
        axisrange  = configHistosProcs["range"]
        rebin      = configHistosProcs["rebin"]

        #DO NOT DO THIS IF THE HISTOGRAM WAS ORIGINALLY A TH2* or one where the post_range is just 0
        if not axisrange:
            return histo

        if configHistosProcs["type"] == "TH2F" or configHistosProcs["type"] == "TH2D":
            return histo

        histo.GetXaxis().SetRangeUser(axisrange[0], axisrange[1])
        
        histoname = histo.GetName()
        #create the bins
        if configHistosProcs["useBinvec"]:
            binarray = configHistosProcs["BinArray"]
            nbins = len(binarray)-1
            #print("Rebinning with array {}".format(binarray))
            histo.GetXaxis().SetRangeUser(axisrange[0], axisrange[1])
            hist_binned = histo.Rebin( nbins,histoname+"_binned", binarray  )
            return hist_binned
            #hist_binned = TH1D(histoname+"_binned",histo.GetTitle(), nbins, binarray )

        histo.SetName( histoname+"_binned" )
        return  histo


    def HistRangeBinning_old(self, histo, configHistosProcs):
        axisrange  = configHistosProcs["range"]
        rebin      = configHistosProcs["rebin"]
        #DO NOT DO THIS IF THE HISTOGRAM WAS ORIGINALLY A TH2* or one where the post_range is just 0
        
        if not axisrange:
            return histo
        if configHistosProcs["type"] == "TH2F" or configHistosProcs["type"] == "TH2D":
            return histo

        if axisrange:
            histo.GetXaxis().SetRangeUser(axisrange[0], axisrange[1])
        
        compare_integrals = {}
        compare_integrals["beforebinning"] = histo.Integral()
        histoname = histo.GetName()
        histo.SetName(histoname+"_old")

        #create the bins
        if configHistosProcs["useBinvec"]:
            binarray = configHistosProcs["BinArray"]
            nbins = len(binarray)-1
            hist_binned = TH1D(histoname+"_binned",histo.GetTitle(), nbins, binarray )
        else:
            nbins= histo.GetXaxis().GetNbins()
            binlow = histo.GetXaxis().GetBinLowEdge(histo.GetXaxis().GetFirst())
            binup = histo.GetXaxis().GetBinUpEdge(histo.GetXaxis().GetLast())
            hist_binned = TH1D(histoname+"_binned",histo.GetTitle(), nbins, binlow, binup )
        
        #apparently here, the histo is replaced with another one with the same name...
        #it's not really a problem, but still
            #########=> this might also be fixed when just using the rebnning instead

        bins_of_old_hist = range( histo.GetXaxis().GetFirst(), histo.GetXaxis().GetLast()  +1)

        #get the size of delta from the binwidth of the original histogram divided by 10 
        delta = 0.1* histo.GetXaxis().GetBinWidth( histo.GetXaxis().GetFirst())
        if histo.GetXaxis().GetBinWidth( histo.GetXaxis().GetFirst()) - histo.GetXaxis().GetBinWidth( histo.GetXaxis().GetLast()) >0.000001:
            print("WARNING: this only works for a histogram that is initally evenly binned!")

        newh_bins = range( hist_binned.GetXaxis().GetFirst(), hist_binned.GetXaxis().GetLast() +1)
        for bin in newh_bins:
            nevts = 0
            for oldbin in bins_of_old_hist:
                if histo.GetXaxis().GetBinLowEdge(oldbin) >= hist_binned.GetXaxis().GetBinLowEdge(bin) -delta and histo.GetXaxis().GetBinUpEdge(oldbin) <= hist_binned.GetXaxis().GetBinUpEdge(bin)+ delta:
                    nevts += histo.GetBinContent(oldbin)
                    #if the bin is completely inbetween the new edges, add its content to nevts
            hist_binned.SetBinContent( bin , nevts  )
        compare_integrals["afterbinning"] = hist_binned.Integral()
        if self.verbose:
            print("Integrals of histograms before binning {} and after binning {} ".format(compare_integrals["beforebinning"], compare_integrals["afterbinning"]  ))    



        #hist_binned = histo.Clone()
        #hist_binned.Rebin(rebin)
        #hist_binned.GetXaxis().SetRangeUser(axisrange[0], axisrange[1])

        histo.Delete()
        return hist_binned.Clone()
