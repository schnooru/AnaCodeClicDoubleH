from ROOT import TFile,gDirectory,gROOT, TH1F, TH2F, TH2D
import sys
from HHTools import dictAnaSample_FromCSV
from clicpyana.ReadingTools import ReaderTools
from clicpyana.ReadingTools.ReadDatasetHistos import ReadDatasetHistos
from clicpyana.AnalysisTools import NormalizeHistos
import os.path
from processHistosDoubleH import DSID_from_hname, RetrieveHistos_FromFilePath,readHistosInNormHisto
from array import array

# file nocuts:     runConfig["Proc_BG_nocuts_histoFile"]
# file selection:  runConfig["Proc_BG_BDT_histoFile"]

def PrintHistoBinContents(histo):
 

    printout = "Bin contents of histo {}:\n".format(histo.GetName())
    printout += "#bin, bin_start, bin_end, content\n"
    for b in xrange(histo.GetNbinsX()+2):
        if b ==0 :
            continue
        binstart   = histo.GetXaxis().GetBinLowEdge(b)
        binend     = histo.GetXaxis().GetBinUpEdge(b)
        bincontent = histo.GetBinContent(b)

        printout   += "{}, {}, {}, {}\n".format(b,binstart,binend,bincontent)
    return printout

def DictHistoBinContents(histo):
    contentdict = {}
    for b in xrange(histo.GetNbinsX()+2):
        if b ==0 :
            continue
        binstart   = histo.GetXaxis().GetBinLowEdge(b)
        binend     = histo.GetXaxis().GetBinUpEdge(b)
        bincontent = histo.GetBinContent(b)
        contentdict [b] = [binstart, binend, bincontent]

    return contentdict

def CouplingsFromDSID(DSID, confPath, runConfig):

    samplesDict_Sig = dictAnaSample_FromCSV( confPath + runConfig["Run_Sig_DsidCsvFile"])

    gHHH  = samplesDict_Sig[DSID]["couplings"][0]
    gHHWW = samplesDict_Sig[DSID]["couplings"][1]
    return "({},{})".format(gHHH,gHHWW)


    
def TableFromHistoDict(histodict,runConfig,histoConfigDict,oneproc = "ee2qqqq",printCouplings=False)    :
    printout = ""
    binarray = GetBinArray(runConfig,histoConfigDict,"SumMj")
    nbins    = len(binarray)-1
    procDictBinContents = {}

    #go through the processes and fill the  dict of bin contents for each
    list_of_processes = []
    for h in histodict.keys():
        process = h
        list_of_processes.append(h)
        procDictBinContents[h] = {}
        for i in histodict[h]:
            if i.GetName().startswith("SumMj") and not i.GetName().startswith("SumMjvs"):
                HistoContentsDict = DictHistoBinContents(i)
        procDictBinContents[h] = HistoContentsDict
    #print procDictBinContents
    #then, create a table
    firstline = "#bin, bin_start, bin_end, {}\n".format(" ,".join(list_of_processes))
    confPath = runConfig["ConfigFolder"]
    secondline = ""
    list_of_Couplings = []
    if printCouplings:
        for i in list_of_processes:
            list_of_Couplings.append( CouplingsFromDSID(i,confPath, runConfig ))
        secondline = "#bin, bin_start, bin_end, (gHHH,gHHWW)= {}\n".format(" ,".join(list_of_Couplings))
#    print(secondline)
    printout += firstline
    printout +=secondline
    tablelines = []


    for bin_n in xrange(nbins):
        b = bin_n+1
        #print(procDictBinContents["ee2HHbbbb"] )
        bin_start = procDictBinContents[oneproc][b][0]
        bin_end   = procDictBinContents[oneproc][b][1]  
        lineprint = "{:4}, {:9}, {:7}".format(b,bin_start,bin_end)
        for proc in list_of_processes:
            procdict = procDictBinContents[proc]
            if bin_start == procdict[b][0]:
                lineprint += ", {:.7f}".format(procdict[b][2] )            
        tablelines.append(lineprint)

    printout += "\n".join(tablelines)

    printout += "\n"
    return printout


    

def GetBinArray(runConfig,histoConfigDict,histname):
    
    configHistos = histoConfigDict[histname]
    binarray = array('d')
    [binarray.append(x) for x in configHistos["binvec"]]
    configHistos["BinArray"] = binarray


    #print("Rebinning to {}".format(configHistos["binvec"]))
    return binarray

    
def WriteBinnedNevents(groupedHistos,runConfig,histoConfigDict,procname=""):

    for h in groupedHistos.keys():
        for i in groupedHistos[h]:
            if i.GetName().startswith("SumMj") and not i.GetName().startswith("SumMjvs")  and i.GetName().endswith(procname): # and i.GetName().endswith("ee2HHbbbb"):
                print PrintHistoBinContents(i)

    return 


def RebinHistos(HistosDict_orig, runConfig,histoConfigDict):
    HistosDict_rebinned = {}
    for h in HistosDict_orig.keys():
        HistosDict_rebinned[h] = []
        for i in xrange(len(HistosDict_orig[h])):
            hist_i = HistosDict_orig[h][i]
            if hist_i.GetName().startswith("SumMj") and not hist_i.GetName().startswith("SumMjvs"): # and hist_i.GetName().endswith("ee2HHbbbb"):
                binarray = GetBinArray(runConfig,histoConfigDict,"SumMj")
                nbins    = len(binarray)-1
                histoname = hist_i.GetName()
                i_binned = hist_i.Rebin(nbins,histoname.replace("_"," for "),binarray)
                HistosDict_rebinned[h].append(i_binned)
    return HistosDict_rebinned
    
def RatioHistoGroupDict(NumeratorGroupDict,DenominatorGroupDict):
    HistoDict_Ratio = {}
    for h in NumeratorGroupDict.keys():
        #skip if there is not denominator corresponding the same prodID (because they are only a few selected ones)

        if not h in DenominatorGroupDict:
            continue
        HistoDict_Ratio[h] =[]
        for i in xrange(len(NumeratorGroupDict[h])):
            ratio = NumeratorGroupDict[h][i].Clone()
            #if ratio.GetName().startswith("SumMj") and ratio.GetName().endswith("ee2HHbbbb"):
            #    print("vorher: {}/{}".format(ratio.GetBinContent(200),  DenominatorGroupDict[h][i].GetBinContent(3)))
            ratio.Divide( DenominatorGroupDict[h][i] )
            #if ratio.GetName().startswith("SumMj") and ratio.GetName().endswith("ee2HHbbbb"):
            #    print("nachher: {}".format(ratio.GetBinContent(200)))
            HistoDict_Ratio[h].append(ratio)
    return HistoDict_Ratio

def ApplyAnaVarFactor(hdict_selec,efficiency  ):
    hdict_selec_factor = {}
    for h in hdict_selec.keys():
        hdict_selec_factor[h] = []
        for i in xrange(len(hdict_selec[h])):
            histo = hdict_selec[h][i]
            dsid = DSID_from_hname(histo.GetName())
            
            histo.Scale(efficiency[dsid])
            hdict_selec_factor[h].append(histo)
    return hdict_selec_factor
    

def EfficienciesAnaChain(runConfig,histoConfigDict,NormHisto_nonorm, NormHisto,groupConfig_Sig_selected):
    efficiencydict = {}
    hdict_selec = NormHisto_nonorm.histos_BDT_Sig

    #print hdict_selec
    grouped_hdict_nocuts = NormHisto_nonorm.GroupHistograms(NormHisto_nonorm.histos_nocuts_Sig_theo, groupConfig_Sig_selected)
    #print("\n\n\nAfter {}".format(grouped_hdict_nocuts))
    
    grouped_hdict_selec  = NormHisto_nonorm.GroupHistograms(hdict_selec,  groupConfig_Sig_selected  )

    grouped_hdict_nocuts = RebinHistos(grouped_hdict_nocuts,runConfig,histoConfigDict)
    grouped_hdict_selec  = RebinHistos(grouped_hdict_selec, runConfig,histoConfigDict)

    #print ("This should only contain the selected dsids {}".format(grouped_hdict_nocuts))
    efficiencydict = RatioHistoGroupDict(grouped_hdict_selec,  grouped_hdict_nocuts )

    #print("Bin contents normalized to luminosity {} and polarization {}:".format(runConfig["Lumi"],runConfig["Polarization_factor_SIG"]))
    #WriteBinnedNevents(efficiencydict,runConfig,histoConfigDict)
    return efficiencydict


def Efficiencies(runConfig,histoConfigDict,NormHisto_nonorm, NormHisto,groupConfig_BG):
    #fill a histogram dictionary with the selec/nocuts*eff_anavar values
    eff_anavar = {'6025': 0.302 , '6776': 0.7460 ,  '6751': 0.9372 ,  '6754': 0.2763 ,  '6540': 0.4110 ,  '2660': 0.9480 ,  '3048' : 0.4613 ,  '4586' : 0.8317 ,  '6747' : 0.4210 ,  '6785': 0.8320 ,  '6782': 0.8212 ,  '6779': 0.8173 ,  '6736' : 0.3876 ,  '6742' : 0.3870 ,  '6733' : 0.2352 ,  '6739' : 0.2361 ,  '6574' : 0.9024 ,  '6580' : 0.9024 ,  '6571' : 0.5426 ,  '6577' : 0.5431 ,  '6942' : 0.4684 ,  '6945' : 0.7676 ,  '6948' : 0.4676 ,  '6951' : 0.7677 ,  '6730': 0.8315 ,  '6727': 0.5449 ,  '6724': 0.5443 ,  '6721' : 0.3710 ,  '6724' : 0.,  'HHvvToOthers' : 0.  }

    #not grouping the samples of different dsids belonging to one process:
    #hdict_nocuts = NormHisto.histos_nocuts_BG
    #hdict_selec  = NormHisto.histos_BDT_BG
    #WriteBinnedNevents(hdict_nocuts,runConfig,histoConfigDict,NormHisto)
    #WriteBinnedNevents(hdict_selec, runConfig,histoConfigDict,NormHisto)

    #print     NormHisto_nonorm.histos_BDT_BG["6025"][1].GetBinContent(20)

    #first rebin
    #then apply eff_anavar according to dsid
    #then group the dsids together
    #this order is also problematic because the efficiencies cannot be added! so... how else?
    #apply eff_anavar before grouping to the numerator histogram, i.e. the histos_BDT_BG
    
    hdict_selec = NormHisto_nonorm.histos_BDT_BG
    hdict_selec_anavar = ApplyAnaVarFactor(hdict_selec, eff_anavar  )
    
    grouped_hdict_nocuts = NormHisto.GroupHistograms(NormHisto.histos_nocuts_BG,        groupConfig_BG)
    grouped_hdict_selec  = NormHisto.GroupHistograms(hdict_selec_anavar,    groupConfig_BG)

    #print grouped_hdict_selec
    grouped_hdict_nocuts = RebinHistos(grouped_hdict_nocuts,runConfig,histoConfigDict)
    grouped_hdict_selec  = RebinHistos(grouped_hdict_selec, runConfig,histoConfigDict)

    print("Bin contents normalized to luminosity {} and polarization {}:".format(runConfig["Lumi"],runConfig["Polarization_factor_SIG"]))



    ratioHistoDict = RatioHistoGroupDict(grouped_hdict_selec,  grouped_hdict_nocuts )
    #other format:
#    WriteBinnedNevents(ratioHistoDict,runConfig,histoConfigDict)

    

    
    return ratioHistoDict



def main(*args):
    if len(args) > 1:
        runConfigFile = args[1]
    else:
        sys.exit("Please provide a run configuration file as argument, e.g. ../Configs/runConfig_2016.json")
        
    print("Using this config file: {}".format(runConfigFile))
 
    runConfig = ReaderTools.configDictFromJson(runConfigFile)
    confPath = runConfig["ConfigFolder"]
    groupConfig_BG = ReaderTools.configDictFromJson(confPath + runConfig["Proc_BG_Group_Config"])

    #remove ee2HHbbbb because it is not needed
    groupConfig_BG.pop("ee2HHbbbb")


    groupConfig_Sig = ReaderTools.configDictFromJson(confPath + runConfig["Proc_Sig_Group_Config"])

    NormHisto = readHistosInNormHisto(runConfig )

    samplesDict_Sig_theory = dictAnaSample_FromCSV(confPath + runConfig["Diff_theory_selected_DsidCsvFile"] ) 

    groupConfig_Sig_selected = {}
    for  i in samplesDict_Sig_theory:
        groupConfig_Sig_selected[str(i)] = [int(i)]
#    groupConfig_Sig_selected = {'7169': [7169], '8209': [8209], '8185': [8185], '7175': [7175], '7189': [7189], '7181': [7181], '7183': [7183], '7179': [7179]}

    
    histos_nocuts_Sig_theory = RetrieveHistos_FromFilePath( runConfig["Diff_theory_Sig"]   , samplesDict_Sig_theory)
    NormHisto.SetTheorySigNocuts(histos_nocuts_Sig_theory)
    NormHisto.NormalizeTheorySigNocuts(samplesDict_Sig_theory)

    #print    NormHisto.histos_BDT_BG_nonorm["6025"][1].GetBinContent(20)
    histoConfigDict = ReaderTools.configDictFromJson(confPath + runConfig["Run_Histo_Config"])


    
    print("The grouped Background event yields for lumi {}/fb".format(NormHisto.lumi))
    print NormHisto.GroupResultsTable(NormHisto.histos_BDT_BG, groupConfig_BG)


    groupedHistos_BG = NormHisto.GroupHistograms(NormHisto.histos_BDT_BG, groupConfig_BG)
    groupedHistos_BG = RebinHistos(groupedHistos_BG,runConfig,histoConfigDict)
    print("Bin contents normalized to luminosity {} and polarization {}:".format(runConfig["Lumi"],runConfig["Polarization_factor_SIG"]))
    #other format:
    #WriteBinnedNevents(groupedHistos_BG,runConfig,histoConfigDict)

    print("====== Background event yields:\n")
    print TableFromHistoDict( groupedHistos_BG,runConfig ,histoConfigDict)
    selection = "looseBDT"
    selection = "tightBDT"
    #binning = "5bins"
    binning = "9bins"
    binning = "1bin"


    outfile_name = "DifferentialInfo/Eventyields_{}_{}.txt".format( selection, binning )
    outfile = open(outfile_name, "w")
    outfile.write(TableFromHistoDict( groupedHistos_BG,runConfig ,histoConfigDict))
    outfile.close()
    print("Wrote Eventyields to {}".format(outfile_name))


    print("====== Background efficiencies: (reco-level after selection)/(reco-level before selection)\n")
    NormHisto_nonorm = readHistosInNormHisto(runConfig, True )
    histos_nocuts_Sig_theory_nonorm = RetrieveHistos_FromFilePath( runConfig["Diff_theory_Sig"]   , samplesDict_Sig_theory)
    NormHisto_nonorm.SetTheorySigNocuts(histos_nocuts_Sig_theory_nonorm)
    NormHisto_nonorm.NormalizeTheorySigNocuts(samplesDict_Sig_theory)

    efficienciesdict = Efficiencies(runConfig,histoConfigDict, NormHisto_nonorm,NormHisto,groupConfig_BG)

    outfile_name= "DifferentialInfo/Efficiencies_{}_{}.txt".format( selection, binning )
    outfile = open(outfile_name, "w")
    outfile.write(TableFromHistoDict(efficienciesdict,runConfig ,histoConfigDict ))
    outfile.close()
    print("Wrote Efficiencies to {}".format(outfile_name))


    #Now, for the couplings signals:
    #do I actually have all the information?
    # eventyields should be easy enough, start with this
    print("====== Signal event yields:\n")
    groupedHistos_Sig = NormHisto.GroupHistograms(NormHisto.histos_BDT_Sig, groupConfig_Sig_selected  )
    groupedHistos_Sig = RebinHistos(groupedHistos_Sig,runConfig,histoConfigDict)
    outfile_name = "DifferentialInfo/EventyieldsSignals_{}_{}.txt".format( selection, binning )
    outfile = open(outfile_name, "w")
    outfile.write( TableFromHistoDict( groupedHistos_Sig,runConfig ,histoConfigDict,oneproc="7181",printCouplings=True))
    outfile.close()
    print("Wrote Eventyields to {}".format(outfile_name))

    print("====== Signal Efficiencies: (reco-level after selection)/(truth-level before selection)")
    #WriteBinnedNevents(groupedHistos_Sig,runConfig,histoConfigDict) #prints all of them
    WriteBinnedNevents(groupedHistos_Sig,runConfig,histoConfigDict,"7181")

    #efficiencies from anaChain

    #just the number left / totalnumber
    efficienciesdict_sig = EfficienciesAnaChain(runConfig,histoConfigDict, NormHisto_nonorm,NormHisto,groupConfig_Sig_selected)


    outfile_name= "DifferentialInfo/EfficienciesSignals_{}_{}.txt".format( selection, binning )
    outfile = open(outfile_name, "w")
    outfile.write(TableFromHistoDict(efficienciesdict_sig,runConfig ,histoConfigDict, oneproc="7181",printCouplings=True ))
    outfile.close()
    print("Wrote Efficiencies to {}".format(outfile_name))



    
    
if __name__ == "__main__":
    main(*sys.argv)


    
