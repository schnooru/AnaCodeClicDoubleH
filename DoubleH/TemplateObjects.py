from ROOT import TH2F, THStack, gPad , TH1F

class TemplateObjects:
    """ Holds all the information and histograms needed for the plotting and the template fit"""

    def __init__(self, SignalCSVdict, SignalGroupDict, wgtdBackgroundHistos, wgtdSignalHistos, histoConfigDict):
        
        self.histos_Sig        = wgtdSignalHistos
        self.histos_BG         = wgtdBackgroundHistos
        
        self.histoConfigDict   = histoConfigDict
        self.SigCouplingsDict  = SignalCSVdict
        self.SigGroupDict      = SignalGroupDict

        return

    #functions :
        # other useful stuff?

    def gHHH_from_DSID( self, DSID ):
        return float( self.SigCouplingsDict[DSID]["couplings"][0]  )

    def gHHWW_from_DSID( self, DSID ):
        return  float( self.SigCouplingsDict[DSID]["couplings"][1]  )

    def DSID_from_gHHH_gHHWW( self, gHHH, gHHWW ):

        for DSID in  self.SigGroupDict:

            if self.SigCouplingsDict[str(DSID)]["couplings"][0]  == gHHH and self.SigCouplingsDict[str(DSID)]["couplings"][1] == gHHWW:
                return DSID
        return 0
            
    def xsec_from_DSID( self, DSID ):
        return  float( self.SigCouplingsDict[DSID]["xsec"] )

    def DsidList1D( self):
        """returns a list of those DSIDs which are actually gHHWW=1, i.e. those needed for 1D fit"""
        DSIDs_1D = []
        for dsid in self.SigGroupDict:
            if self.gHHWW_from_DSID(dsid) == 1.0:
                DSIDs_1D.append(dsid)
        return DSIDs_1D

    def DsidList2D( self):
        """returns a list of all DSIDs i.e. those needed for 2D fit"""
        DSIDs_2D = []
        for dsid in self.SigGroupDict:
            DSIDs_2D.append(dsid)
        return DSIDs_2D
  
    